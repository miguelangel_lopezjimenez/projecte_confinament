# MANUAL TÈCNIC DE OPENVPN EN PFSENSE

## CONFIGURACIÓ DE SERVIDOR OPENVPN

**1-** Primer tenim que anar a **"VPN"** -> **"OpenVPN"**.

![](assets/openvpn-malj-e228465c.png)

**2-** Ara anirem a la finestra de **"Wizards"**.

![](assets/openvpn-malj-80c196d9.png)

**3-** Ara li donarem a **"Next"** per a començar la configuració.

![](assets/openvpn-malj-64773190.png)

**4-** Ara crearem la **CA (Certificate Authority)** que utilitzarem.

![](assets/openvpn-malj-03e3b48d.png)

**5-** Ara crearem el certificat per el servidor.

![](assets/openvpn-malj-192b8f88.png)

**6-** En aquesta part de la configuració ho deixarem com està per defecte. Es molt important que el port sigui el 1194 i que el protocol sigui **UDP**.

![](assets/openvpn-malj-9a91c693.png)

**7-** Aquest apartat ho deixarem tal i com surt per defecte.

![](assets/openvpn-malj-3699f0b6.png)

**8-** Ara indicarem quina serà la xarxa que utilitzarà la **VPN** i quina és la xarxa local.

![](assets/openvpn-malj-3e069db0.png)

**9-** Ara li direm que utilitzi direccions IPs dinàmiques.

![](assets/openvpn-malj-41916121.png)

**10-** Una vegada fet això baixem al final i li donem a **"Next"**.

![](assets/openvpn-malj-826350a6.png)

**11-** Ara marquem les caselles de **"Firewall Rule"** per afegir a la **WAN** la regla que permet el trafic de la **VPN** i la de **"OpenVPN rule"** per afegir la interfície de la **VPN**.

![](assets/openvpn-malj-e0171480.png)

**12-** Per últim li donem a **"Finish"**.

![](assets/openvpn-malj-584f27a2.png)

**13-** Ara crearem l'usuari que utilitzara per la **VPN**, per fer això anirem a **"System"** -> **"User Manager"**.

![](assets/openvpn-malj-b0042b13.png)

**14-** Ara introduïm les dades del nostre usuari.

![](assets/openvpn-malj-3d70afae.png)

**15-** Ara baixem i marquem la casella de **"Certificate"** i omplim les dades i li donem a **"Save"**.

![](assets/openvpn-malj-792e59aa.png)

**16-** Ara descarregarem el modul que permet exportar el certificat al nostre client. Per fer això tenim que anar a **"System"** -> **"Package Manager"**.

![](assets/openvpn-malj-1cb66d87.png)

**17-** Ara anirem a **"Avaiable packages"** i busquem el paquet **"openvpn-client-export"** i li donem a **"Install"**.

![](assets/openvpn-malj-41d79575.png)

**18-** A continuació anirem a **"VPN"** -> **"OpenVPN"**.

![](assets/openvpn-malj-e228465c.png)

**19-** Ara anem a **"Client Export"**.

![](assets/openvpn-malj-a123481d.png)

**20-** Marcarem la casella de **"Legacy Client"**.

![](assets/openvpn-malj-8cef8784.png)

**21-** Ara anem a l'apartat **"OpenVPN Clients"** i en **"Inline Clients"** li donem a **"Most Clients"** per descarregar el certificat i amb aquest poder-nos connectar a la VPN.

![](assets/openvpn-malj-2120cc33.png)
