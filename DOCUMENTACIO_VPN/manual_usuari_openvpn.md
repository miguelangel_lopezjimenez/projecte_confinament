## CONNECTAR-SE A LA VPN EN UBUNTU

**NOTA:** Per poder realitzar els següents passos necessitaràs que el teu administrador t'hagui proporcionat l'arxiu de configuració per a la **VPN** i en cas de tenir qualsevol problema seguint els següents passos contactar amb el teu administrador per solucionar-los..

**1-** instal·larem el paquet _**network-manager-openvpn-gnome**_ utilitzant la següent comanda:

~~~
sudo apt-get install network-manager-openvpn-gnome
~~~

**2-** Ara a la icona de **"Network Manager"** -> **"VPN"** -> símbol **"+"** (afegir)-> **"Importa des d'un fitxer"**.

![](assets/openvpn-malj-8ca8a05b.png)

**3-** Ara per configurar uns paràmetres, per fer això li donarem a **"Avançat"**.

![](assets/openvpn-malj-9d3265d5.png)

**4-** En la finestra **"General"** marcarem la casella que surt a la imatge.

![](assets/openvpn-malj-0d375f03.png)

**5-** En la finestra de **"Seguretat"** ho deixarem com surt en la imatge. Fet això li donem a **"D'acord"**.

![](assets/openvpn-malj-e6517514.png)

## CONNECTAR-SE A LA VPN EN WINDOWS

**NOTA:** Abans d'intentar connectar-se a la **VPN** des de Windows crearem un nou usuari i un nou certificat per aquest.

**USUARI MLOPEZ2**

![](assets/openvpn-malj-9ce053a5.png)

**CERTIFICAT USUARI MLOPEZ2**

![](assets/openvpn-malj-f586f808.png)

Windows de forma nativa suporta els següents tipus de VPN:

- Protocolo túnel punto a punto (PPTP)
- L2TP/IPsec con certificado
- L2TP/IPsec con clave previamente compartida
- Protocolo de túnel de sockets seguros (SSTP)
- IKEv2

**NOTA:** Per Windows tenim dos programes el tradicional de **"OpenVPN"** o **"OpenVPN Connect"**, ela segona opció és més amigable amb l'usuari però en cas de no funcionar utilitzar la primera opció.

**INSTAL·LACIÓ DEL SOFTWARE OPENVPN EN WINDOWS 10**

**1-** Primer anirem a la pàgina https://openvpn.net/community-downloads/ i descarregarem el paquet corresponent a **Windows 10**.

![](assets/openvpn-malj-db1c3048.png)

**2-** Ara descarreguem l'instal·lador del certificat de **Pfsense** per a Windows 10.

![](assets/openvpn-malj-75d04aa6.png)

**3-** Una vegada descarregat l'instal·lem.

![](assets/openvpn-malj-4b2a4840.png)

**4-** Ara fem click dret a la icona de la aplicació de **OpenVPN** i li donem a **"Conectar"**.

![](assets/openvpn-malj-199d7856.png)

**5-** Per últim introduïm les credencials i li donem a **"OK"**.

![](assets/openvpn-malj-ec98bb70.png)

**INSTAL·LACIÓ DEL SOFTWARE OPENVPN CONNECT EN WINDOWS 10**

**1-** Anem al següent enllaç i descarregem la aplicació "OpenVPN Connect" https://openvpn.net/client-connect-vpn-for-windows/, li donem a "DOWNLOAD OPENVPN CONNECT"

![](assets/manual_usuari_openvpn-abda54b2.png)

**2-** Una vegada obert el programa d'instal·lació que acabem de descarregar s'obrira una finestra, li donem a "Next".

![](assets/manual_usuari_openvpn-c6da1438.png)

**3-** Ara ens demanarà acceptar els terminis d'us. Marquem la casella i li donem a **"Next"**.

![](assets/manual_usuari_openvpn-dba3078a.png)

**4-** Per últim li donem a **"Install"**.

![](assets/manual_usuari_openvpn-cb90568d.png)

**5-** Ara obirm el programa i ens situem sobre l'apartat **"FILE"** i li donem a **"BROWSE"** per buscar l'arxiu de configuració que ens ha sigut proporcionat..

![](assets/manual_usuari_openvpn-1ef87cda.png)

**6-** Una vegada trobat l'arxiu introduïm l'usuari i contrasenya que ens a sigut proporcionats, marquem la casella **"Connect after import"** per connectar-nos quan afegim la **VPN** i finalment li donam a **"Add"**.

![](assets/manual_usuari_openvpn-38876904.png)

**7-** Una vegada connectats en aquesta finestra podem veure tota la informació del trafic que estem generant a la **VPN**.

![](assets/manual_usuari_openvpn-ddfeaa37.png)
