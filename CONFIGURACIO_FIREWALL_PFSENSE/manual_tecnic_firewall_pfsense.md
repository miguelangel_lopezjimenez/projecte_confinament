# MANUAL TÈCNIC INSTAL·LACIO I CONFIGURACIÓ FIREWALL PFSENSE

## INSTAL·LACIÓ PFSENSE

**1-** Una vegada creada la màquina virtual e iniciada la ISO el primer que ens surt es si acceptem el termes d'us:

![](assets/practica-firewall-malj-cc7c053a.png)

Li donem a **"Accept"** i continuem.

**2-** Ara ens pregunta que volem fer, en el nostre cas es instal·lar **Pfsense** per fer això escollim la opció **"Install"**.

![](assets/practica-firewall-malj-c16065c0.png)

**3-** Ara ens pregunta per la distribució de teclat, podem deixar-lo per defecte o escollir la que volem.

![](assets/practica-firewall-malj-87d9968e.png)

**4-** Ara podem escollir utilitzar el disc sencer i que s'encarregui de fer tot el sistema o fer les particions manualment.

![](assets/practica-firewall-malj-dbbcbe25.png)

**5-** Ara ens informa de que la instal·lació ha finalitzat i si volem obrir un terminal per fer modificacions manualment, li donem a **"No"**.

![](assets/practica-firewall-malj-15abc5d0.png)

**6-** Una vegada instal·lat li donem a **"Reboot"** i quan estigui reiniciant apaguem la màquina i traíem la ISO.

![](assets/practica-firewall-malj-83c21086.png)

## ASSIGNACIÓ DE ADRECES A LES INTERFÍCIES

**1-** Per configurar les IPs que utilitzarà una interfície en el servidor escollirem la opció **"2) Set interfaces(s) IP address"** i li donem a la tecla **"Enter"**:

~~~
VirtualBox Virtual Machine - Netgate Device ID: a09a8cddadea33e1bced

*** Welcome to pfSense 2.4.4-RELEASE-p3 (amd64) on pfSense ***

 WAN (wan)       -> em0        -> v4/DHCP4: 192.168.0.12/24
 LAN (lan)       -> em1        -> v4: 10.10.0.1/24
 DMZ (opt1)      -> em2        -> v4: 192.168.1.1/24

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option: 2
~~~

**2-** Ara seleccionem la interfície a la qual li volem canviar la IP, en aquest cas la **LAN** que es la **"2"**:

~~~
Available interfaces:

1 - WAN (em0 - dhcp, dhcp6)
2 - LAN (em1 - static)

Enter the number of the interface you wish to configure: 2
~~~

**3-** Ara determinem quina adreça tindrà la interfície **LAN**:

~~~
Enter the new LAN IPv4 address.  Press <ENTER> for none:
> 10.10.0.1
~~~

**4-** Ara ens preguntarà per la mascara de la nostra xarxa, en el nostre cas es **24**:

~~~
Subnet masks are entered as bit counts (as in CIDR notation) in pfSense.
e.g. 255.255.255.0 = 24
     255.255.0.0   = 16
     255.0.0.0     = 8

Enter the new LAN IPv4 subnet bit count (1 to 31):
> 24
~~~

**5-** Ara ens preguntarà per la porta d'enllaç de la IPv4, ho deixem en blanc i li donem a la tecla **"Enter"**:

~~~
For a WAN, enter the new LAN IPv4 upstream gateway address.
For a LAN, press <ENTER> for none:
>
~~~

**6-** Ara ens preguntara la direcció del gateway de versió IPv6 per a la nostra xarxa LAN, en el nostre cas no la utilitzarem i per això li donarem a la tecla **"Enter"**:

~~~
Enter the new LAN IPv6 address.  Press <ENTER> for none:
>
~~~

**7-** Ara ens preguntara si volem habilitar el servidor **DHCP** per a la interfície **LAN**, escrivim **"y"** i li donem a la tecla **"Enter"**:

~~~
Do you want to enable the DHCP server on LAN? (y/n) y
~~~

**8-** Ara en preguntara quina es la direcció inicial del **DHCP**, la introduïm i li donem al **"Enter"**:

~~~
Enter the start address of the IPv4 client address range: 10.10.0.50
~~~

**9-** Ara ens preguntarà en quina direcció volem acabar el **DHCP**, la introduïm i li donem a la tecla **"Enter"**:

~~~
Enter the end address of the IPv4 client address range: 10.10.0.200
~~~

**10-** Ara ens preguntarà si volem utilitzar el protocol **HTTP** per el configurador web, com no ho volem fer escrivim **"n"** i li donem al **"Enter"**:

~~~
Do you want to revert to HTTP as the webConfigurator protocol? (y/n) n
~~~

## ACCEDIR A PFSENSE VIA WEB

**1-** Per accedir via web obrim el navegador i en la barra cercadora posem la direcció del nostre servidor, en aquest cas es https://10.10.0.1:

![](assets/practica-firewall-malj-19777c1b.png)

**2-** Ara ens sortirà una finestra d'alerta per que estem utilitzant un certificat auto firmat, per continuar li donem a **"Avanzado"** i desprès a **"Aceptar el riesgo y continuar"**:

![](assets/practica-firewall-malj-67b3aa18.png)

**3-** Ara ja estem en el menú d'inici de sessió de **Pfsense**. Les credencials son:

**USUARI:** admin

**PASSWORD:** pfsense

![](assets/practica-firewall-malj-8d3af0f5.png)

Una vegada iniciada sessió ja podem començar a administrar el nostre firewall via web.

## CREAR REGLES PER AL FIREWALL

De exemple crearem la regla que permet el trafic web per **HTTPS** des de la **WAN** fins a la **DMZ**.

**1-** Una vegada iniciada sessió via web en **Pfsense** tenim que anar a **"Firewall"** -> **"Rules"**.

![](assets/practica-firewall-malj-dc536a5e.png)

**3-** Ara per agregar una nova regla li donem al botó **"Add"** com surt en la imatge:

![](assets/practica-firewall-malj-2ceeb1c6.png)

**4-** En la part de configuració de la regla **"Edit Firewall Rule"** en **"Action"** seleccionem **"Pass"** per que volem deixar passar el trafic cap a uns ports en concret, en **"Interface"** **DMZ** per determina en quina interfície s'aplicara, en **"Address Family"** IPv4 i en **"Protocol"** **"TCP"**.

![](assets/manual_tecnic_firewall_pfsense-16455f7a.png)

**5-** En l'apartat de **"Source"** escollim **"WAN net"**.

![](assets/manual_tecnic_firewall_pfsense-862f9b97.png)

**6-** En **"Destination"** seleccionem **"Single host or alias"** i afegim la **IP** destinatari, en aquest cas la del servidor web "192.168.1.5" i seleccionem el port de desti en aquest cas "HTTPS (443)"

![](assets/manual_tecnic_firewall_pfsense-0e0ac4f5.png)

**7-** Ara anem a baix de la pàgina i li donem a **"Save"** per guardar els canvis.

![](assets/practica-firewall-malj-0a8f2f13.png)

**8-** Per últim per aplicar la regla li tenim que donar al botó **"Apply Changes"** per aplicar-la.

![](assets/practica-firewall-malj-1a594a1c.png)


## REGLES CREADES EN EL FIREWALL

### WAN

![](assets/manual_tecnic_firewall_pfsense-c46eac82.png)

### DMZ

![](assets/manual_tecnic_firewall_pfsense-7273c45f.png)


## CONFIGURACIÓ REENVIAMENTS NAT

Per redirigir el trafic que ens arriba del router a la interfície **WAN** als seus respectius servidor que estan situats a la **DMZ** tenim que redirigir-lo en **Pfsense**. D'exemple utilitzarem el de **HTTPS**:

**1-** Primer anirem a **"Firewall"** -> **"NAT"**.

![](assets/practica-firewall-malj-dc536a5e.png)

**2-** Una vegada dintre li donem al botó **"Add"** situat al final de la pàgina.

![](assets/manual_tecnic_firewall_pfsense-c53feeb9.png)

**3-** Ara configurarem el reenviaments.

![](assets/manual_tecnic_firewall_pfsense-919ffd9e.png)

**NOM**  |  **PER A QUE SERVEIX?**
--|--
**Interface**  |  Aquí seleccionem la interfície de la qual reenviament, en el nostre cas serà sempre **"WAN"**,
Protocol  |  Quin protocol es el que volem reenviar. En el nostre cas "TCP".
**Destination**  |  A on va el tràfic en el nostre cas va a la **"WAN net"**.
**Destination port range**  |  quin es el port del destinatari, en aquest cas **HTTP (443)**.
**Redirect target IP**  |  La **IP** a la qual volem redirigir el tràfic.
**Redirect target port**  |  El port al qual volem redirigir, en aquest cas al **HTTPS (443)**.

**4-** Una vegada configurat el reenviament li donem a **"Save"** per guardar els canvis.

![](assets/manual_tecnic_firewall_pfsense-552fa93d.png)

## TAULA DE TOTS ELS REENVIAMENTS

![](assets/manual_tecnic_firewall_pfsense-0886ce49.png)
