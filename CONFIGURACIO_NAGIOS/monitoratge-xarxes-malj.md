# MANUAL TÈCNIC DE INSTAL·LACIÓ/CONFIGURACIÓ DE NAGIOS

## INSTAL·LACIÓ DE NAGIOS

**1-** Per a instal·lar **NagiosIX** ho podem fer de fues formes, manual o automàtica.

Per fer-lo de manera automàtica executarem la següent comanda:

~~~
usuari@server:~$ curl https://assets.nagios.com/downloads/nagiosxi/install.sh | sudo sh
~~~

Per fer-ho de manera manual executarem les següents comandes:

~~~
cd /tmp
wget https://assets.nagios.com/downloads/nagiosxi/xi-latest.tar.gztar
xzf xi-latest.tar.gz
cd nagiosxi
./fullinstall
~~~

**2-** Ara configurarem el **Nagios** a traves de la interfície web, per fer això obrim el navegador e introduïm la direcció IP del nostre servidor. Una vegada dins ens sortirà aquesta finestra que ens permetrà accedir al **Nagios**.

![](assets/monitoratge-xarxes-malj-d85b8251.png)

**3-** Ara ens preguntarà per la direcció URL que volem que tingui per accedir al **Nagios**, la zona horària, el llenguatge i el mode de la interfície gràfica. Una vegada fet això li donem a **"Next"** per continuar.

![](assets/monitoratge-xarxes-malj-e54edc9c.png)

**4-** Ara ens preguntarà per la contrasenya, el nom complert del usuari administrador i per una direcció de correu electrònic. Una vegada fet això li donem a **"Finish install"** per acabar la instasl·lació.

![](assets/monitoratge-xarxes-malj-6ad5fb38.png)

## CONFIGURAR UNA TASCA D'AUTO DESCOBRIMENT

**1-** Per crear una tasca d'auto descobriment primer tenim que anar a **"Configurar"** -> **"Tareas de Auto-descubrimiento"**.

![](assets/monitoratge-xarxes-malj-28851d8e.png)

**2-** Ara li donem a **"+ Tareas de Auto-descubrimiento"**.

![](assets/monitoratge-xarxes-malj-af6e777c.png)

**3-** Ara configurarem la nova tasca.

![](assets/monitoratge-xarxes-malj-b94b588f.png)

**4-** Una vegada acaba d'executar la tasca podem veure els dispositius que a trobats donant-li a **"Nuevo"**.

![](assets/monitoratge-xarxes-malj-4c085136.png)

**5-** Ara li donem a **"Proximo"** per a continuar.

![](assets/monitoratge-xarxes-malj-d3c5ea47.png)

**6-** Aquí veiem un resum dels dispositius trobats i els serveis que tenen habilitats. Li donem a **"Next"**.

![](assets/monitoratge-xarxes-malj-2e3ec971.png)

**7-** Aquí podem seleccionar el temps que volem que trigui a comprovar l'estat dels dispositius. Fet això li donem a **"Finalizar"**.

![](assets/monitoratge-xarxes-malj-2d5a30fa.png)

**8-** Per veure els dispositius connectats i els serveis que tenen tenim que anar a **"Vistas"**.

**SERVEIS**

![](assets/monitoratge-xarxes-malj-7cead54f.png)

![](assets/monitoratge-xarxes-malj-9efb4b60.png)

![](assets/monitoratge-xarxes-malj-c1aed303.png)

**DISPOSITIUS**

![](assets/monitoratge-xarxes-malj-8a1bf579.png)
