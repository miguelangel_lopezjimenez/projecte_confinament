# DOCUMENTACIÓ COMPLETA DEL PROJECTE

# MAPA LÒGIC

![](assets/mapa_logic.png)

# CREDENCIALS DELS SERVEIS CONFIGURATS

## DOMINI DNS DE LA WEB
projecte-malj.servehttp.com

## FTP AMB SSL CERTIFICAT AUTOSIGNAT
**CONNEXIÓ:** projecte-malj.servehttp.com

**USUARI:** professor

**PASSWORD:** P@ssw0rd124.

## VPN (DOCUMENT PUJAT A LA ENTREGA I AL FTP)
**USUARI:** professor

**PASSWORD:** P@ssw0rd124.

## MAIL AMB SSL DE CERTIFICAT AUTOSIGNAT I SPAMASSASIN
**CORREU:** projecte-malj.servehttp.com

**USUARI 1:** pau

**PASSWORD:** P@ssw0rd124.

**USUARI 2:** roger

**PASSWORD:** P@ssw0rd124.

## FIREWALL AMB PFSENSE
**USUARI:** admin

**PASSW0RD:** pfsense

## MONITORATGE AMB NAGIOS
**USUARI:** nagiosadmin

**PASSWORD:** P@ssw0rd124.

# MANUAL TÈCNIC DE CONFIGURACIÓ DEL ROUTER LOWI

## COM ACCEDIR AL ROUTER

Per accedir al router tenim que posar la IP del nostre router al navegador web.

![](assets/manual_tecnic_router_lowi-2121150c.png)

## REENVIAMENT DE PORTS

Accedim al router ens posem en **"Modo Avanzado"** d'usuari anem a la finestra de **"Internet"** -> **"Redirección de Puertos"** i li donem al botó **"+"** per afegir un de nou i omplim les dades que ens demana. Una vegada fet per guardar els canvis li donem a **"Aplicar"**.

![](assets/manual_tecnic_router_lowi-27c65be8.png)

### TAULA DE TOTS ELS RENVIAMENTS DEL ROUTER

![](assets/manual_tecnic_router_lowi-018ab4a6.png)

## CONFIGURACIÓ DDNS

Anem a **"DNS & DDNS"** i en **"Proveedor"** seleccionem **"no-ip.com"** i omplim les dades que ens demana. Fet això li donem a **"Aplicar"**.

![](assets/manual_tecnic_router_lowi-ff49fd57.png)
# MANUAL TÈCNIC INSTAL·LACIO I CONFIGURACIÓ FIREWALL PFSENSE

## INSTAL·LACIÓ PFSENSE

**1-** Una vegada creada la màquina virtual e iniciada la ISO el primer que ens surt es si acceptem el termes d'us:

![](assets/practica-firewall-malj-cc7c053a.png)

Li donem a **"Accept"** i continuem.

**2-** Ara ens pregunta que volem fer, en el nostre cas es instal·lar **Pfsense** per fer això escollim la opció **"Install"**.

![](assets/practica-firewall-malj-c16065c0.png)

**3-** Ara ens pregunta per la distribució de teclat, podem deixar-lo per defecte o escollir la que volem.

![](assets/practica-firewall-malj-87d9968e.png)

**4-** Ara podem escollir utilitzar el disc sencer i que s'encarregui de fer tot el sistema o fer les particions manualment.

![](assets/practica-firewall-malj-dbbcbe25.png)

**5-** Ara ens informa de que la instal·lació ha finalitzat i si volem obrir un terminal per fer modificacions manualment, li donem a **"No"**.

![](assets/practica-firewall-malj-15abc5d0.png)

**6-** Una vegada instal·lat li donem a **"Reboot"** i quan estigui reiniciant apaguem la màquina i traíem la ISO.

![](assets/practica-firewall-malj-83c21086.png)

## ASSIGNACIÓ DE ADRECES A LES INTERFÍCIES

**1-** Per configurar les IPs que utilitzarà una interfície en el servidor escollirem la opció **"2) Set interfaces(s) IP address"** i li donem a la tecla **"Enter"**:

~~~
VirtualBox Virtual Machine - Netgate Device ID: a09a8cddadea33e1bced

*** Welcome to pfSense 2.4.4-RELEASE-p3 (amd64) on pfSense ***

 WAN (wan)       -> em0        -> v4/DHCP4: 192.168.0.12/24
 LAN (lan)       -> em1        -> v4: 10.10.0.1/24
 DMZ (opt1)      -> em2        -> v4: 192.168.1.1/24

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option: 2
~~~

**2-** Ara seleccionem la interfície a la qual li volem canviar la IP, en aquest cas la **LAN** que es la **"2"**:

~~~
Available interfaces:

1 - WAN (em0 - dhcp, dhcp6)
2 - LAN (em1 - static)

Enter the number of the interface you wish to configure: 2
~~~

**3-** Ara determinem quina adreça tindrà la interfície **LAN**:

~~~
Enter the new LAN IPv4 address.  Press <ENTER> for none:
> 10.10.0.1
~~~

**4-** Ara ens preguntarà per la mascara de la nostra xarxa, en el nostre cas es **24**:

~~~
Subnet masks are entered as bit counts (as in CIDR notation) in pfSense.
e.g. 255.255.255.0 = 24
     255.255.0.0   = 16
     255.0.0.0     = 8

Enter the new LAN IPv4 subnet bit count (1 to 31):
> 24
~~~

**5-** Ara ens preguntarà per la porta d'enllaç de la IPv4, ho deixem en blanc i li donem a la tecla **"Enter"**:

~~~
For a WAN, enter the new LAN IPv4 upstream gateway address.
For a LAN, press <ENTER> for none:
>
~~~

**6-** Ara ens preguntara la direcció del gateway de versió IPv6 per a la nostra xarxa LAN, en el nostre cas no la utilitzarem i per això li donarem a la tecla **"Enter"**:

~~~
Enter the new LAN IPv6 address.  Press <ENTER> for none:
>
~~~

**7-** Ara ens preguntara si volem habilitar el servidor **DHCP** per a la interfície **LAN**, escrivim **"y"** i li donem a la tecla **"Enter"**:

~~~
Do you want to enable the DHCP server on LAN? (y/n) y
~~~

**8-** Ara en preguntara quina es la direcció inicial del **DHCP**, la introduïm i li donem al **"Enter"**:

~~~
Enter the start address of the IPv4 client address range: 10.10.0.50
~~~

**9-** Ara ens preguntarà en quina direcció volem acabar el **DHCP**, la introduïm i li donem a la tecla **"Enter"**:

~~~
Enter the end address of the IPv4 client address range: 10.10.0.200
~~~

**10-** Ara ens preguntarà si volem utilitzar el protocol **HTTP** per el configurador web, com no ho volem fer escrivim **"n"** i li donem al **"Enter"**:

~~~
Do you want to revert to HTTP as the webConfigurator protocol? (y/n) n
~~~

## ACCEDIR A PFSENSE VIA WEB

**1-** Per accedir via web obrim el navegador i en la barra cercadora posem la direcció del nostre servidor, en aquest cas es https://10.10.0.1:

![](assets/practica-firewall-malj-19777c1b.png)

**2-** Ara ens sortirà una finestra d'alerta per que estem utilitzant un certificat auto firmat, per continuar li donem a **"Avanzado"** i desprès a **"Aceptar el riesgo y continuar"**:

![](assets/practica-firewall-malj-67b3aa18.png)

**3-** Ara ja estem en el menú d'inici de sessió de **Pfsense**. Les credencials son:

**USUARI:** admin

**PASSWORD:** pfsense

![](assets/practica-firewall-malj-8d3af0f5.png)

Una vegada iniciada sessió ja podem començar a administrar el nostre firewall via web.

## CREAR REGLES PER AL FIREWALL

De exemple crearem la regla que permet el trafic web per **HTTPS** des de la **WAN** fins a la **DMZ**.

**1-** Una vegada iniciada sessió via web en **Pfsense** tenim que anar a **"Firewall"** -> **"Rules"**.

![](assets/practica-firewall-malj-dc536a5e.png)

**3-** Ara per agregar una nova regla li donem al botó **"Add"** com surt en la imatge:

![](assets/practica-firewall-malj-2ceeb1c6.png)

**4-** En la part de configuració de la regla **"Edit Firewall Rule"** en **"Action"** seleccionem **"Pass"** per que volem deixar passar el trafic cap a uns ports en concret, en **"Interface"** **DMZ** per determina en quina interfície s'aplicara, en **"Address Family"** IPv4 i en **"Protocol"** **"TCP"**.

![](assets/manual_tecnic_firewall_pfsense-16455f7a.png)

**5-** En l'apartat de **"Source"** escollim **"WAN net"**.

![](assets/manual_tecnic_firewall_pfsense-862f9b97.png)

**6-** En **"Destination"** seleccionem **"Single host or alias"** i afegim la **IP** destinatari, en aquest cas la del servidor web "192.168.1.5" i seleccionem el port de desti en aquest cas "HTTPS (443)"

![](assets/manual_tecnic_firewall_pfsense-0e0ac4f5.png)

**7-** Ara anem a baix de la pàgina i li donem a **"Save"** per guardar els canvis.

![](assets/practica-firewall-malj-0a8f2f13.png)

**8-** Per últim per aplicar la regla li tenim que donar al botó **"Apply Changes"** per aplicar-la.

![](assets/practica-firewall-malj-1a594a1c.png)


## REGLES CREADES EN EL FIREWALL

### WAN

![](assets/manual_tecnic_firewall_pfsense-c46eac82.png)

### DMZ

![](assets/manual_tecnic_firewall_pfsense-7273c45f.png)


## CONFIGURACIÓ REENVIAMENTS NAT

Per redirigir el trafic que ens arriba del router a la interfície **WAN** als seus respectius servidor que estan situats a la **DMZ** tenim que redirigir-lo en **Pfsense**. D'exemple utilitzarem el de **HTTPS**:

**1-** Primer anirem a **"Firewall"** -> **"NAT"**.

![](assets/practica-firewall-malj-dc536a5e.png)

**2-** Una vegada dintre li donem al botó **"Add"** situat al final de la pàgina.

![](assets/manual_tecnic_firewall_pfsense-c53feeb9.png)

**3-** Ara configurarem el reenviaments.

![](assets/manual_tecnic_firewall_pfsense-919ffd9e.png)

**NOM**  |  **PER A QUE SERVEIX?**
--|--
**Interface**  |  Aquí seleccionem la interfície de la qual reenviament, en el nostre cas serà sempre **"WAN"**,
Protocol  |  Quin protocol es el que volem reenviar. En el nostre cas "TCP".
**Destination**  |  A on va el tràfic en el nostre cas va a la **"WAN net"**.
**Destination port range**  |  quin es el port del destinatari, en aquest cas **HTTP (443)**.
**Redirect target IP**  |  La **IP** a la qual volem redirigir el tràfic.
**Redirect target port**  |  El port al qual volem redirigir, en aquest cas al **HTTPS (443)**.

**4-** Una vegada configurat el reenviament li donem a **"Save"** per guardar els canvis.

![](assets/manual_tecnic_firewall_pfsense-552fa93d.png)

## TAULA DE TOTS ELS REENVIAMENTS

![](assets/manual_tecnic_firewall_pfsense-0886ce49.png)

# MANUAL TÈCNIC WEB

## CREACIÓ DEL VIRTUALHOST HTTP

**1-** Primer tenim que instal·lar el servei web **"apache2"**, per fer això utilitzarem la següent comanda:

~~~
usuari@phoenix:~$ sudo apt-get install apache2
~~~

**2-** Una vegada instal·lat crearem el virtual host **HTTP (80)** que redirigirà al **HTTPS (443)**. Per fer això primer tenim que anar al directori _**/etc/apache2/sites-available/**_:

~~~
usuari@phoenix:~$ cd /etc/apache2/sites-available/
~~~

**3-** Una vegada en el directori copiarem l'arxiu **000-default.conf** i li posarem el nom que vulguem, en el meu cas es diu **projecte-malj.conf**:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo cp 000-default.conf projecte-malj.conf
~~~

**4-** Ara obrirem l'arxiu que acabem de crear i l'editarem:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo nano projecte-malj.conf
~~~

**5-** Una vegada dintre afegirem el següent:

~~~
ServerName projecte-malj.servehttp.com

<IfModule mod_rewrite.c>
          RewriteEngine On
          RewriteCond %{HTTPS} off
          RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
       </IfModule>
~~~

**6-** Ara guardem els canvis i sortim, ara habilitarem el modul **"rewrite"** utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2enmod rewrite
~~~

**7-** Ara habilitarem el lloc web utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2ensite projecte-malj,conf
~~~

**8-** Ara ens demanarà que fem restart del **apache**, per fer això utilitzarem la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo service apache2 restart
~~~

## CREACIÓ DE CERTIFICATS VERIFICATS PER UNA CA UTILITZANT CERBOT

**1-** Per instal·lar **"Certbot"** primer afegirem el seu repositori executant les següents comandes:

~~~
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
~~~

**2-** Una vegada afegits el repositori utilitzarem la següent comanda per instal·lar-lo:

~~~
sudo apt-get install certbot python-certbot-apache
~~~

**3-** Ara crearem el nostre certificat i clau que utilitzarem en el següent pas, per fer això utilitzarem la següent comanda:

~~~
sudo certbot certonly --apache
~~~

**NOTA:** Al finalitzar la creació del certificat **"Certbot"** ens donarà les rutes on estan guardats els certificats, estan emmagatzemats a _**/etc/letsencrypt/live/**_.

## CREACIÓ DEL VIRTUALHOST HTTPS

**1-** Primer habilitarem el modul **SSL** de **apache** utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2enmod ssl
~~~

**2-** Ens situem en el mateix directori que amb el altre virtualhost copiarem l'arxiu **default-ssl.conf** i li posarem el nom que vulguem, en el meu cas es diu **projecte-malj-ssl.conf**:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo cp default-ssl.conf projecte-malj-ssl.conf
~~~

**3-** Ara obrirem l'arxiu que acabem de crear i l'editarem:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo nano projecte-malj-ssl.conf
~~~

**4-** Una vegada dintre afegirem el següent:

~~~
<IfModule mod_ssl.c>
       <VirtualHost _default_:443>
               ServerName projecte-malj.servehttp.com
               ServerAdmin webmaster@localhost

               DocumentRoot /var/www/html

               # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
               # error, crit, alert, emerg.
               # It is also possible to configure the loglevel for particular
               # modules, e.g.
               #LogLevel info ssl:warn

               ErrorLog ${APACHE_LOG_DIR}/error.log
               CustomLog ${APACHE_LOG_DIR}/access.log combined

               Header always set Strict-Transport-Security "max-age=63072000;include Subdomains;"

               # For most configuration files from conf-available/, which are
               # enabled or disabled at a global level, it is possible to
               # include a line for only one particular virtual host. For example the
               # following line enables the CGI configuration for this host only
               # after it has been globally disabled with "a2disconf".
               #Include conf-available/serve-cgi-bin.conf

               #   SSL Engine Switch:
               #   Enable/Disable SSL for this virtual host.
               SSLEngine on

               #   A self-signed (snakeoil) certificate can be created by installing
               #   the ssl-cert package. See
               #   /usr/share/doc/apache2/README.Debian.gz for more info.
               #   If both key and certificate are stored in the same file, only the
               #   SSLCertificateFile directive is needed.
               SSLCertificateFile      /etc/letsencrypt/live/projecte-malj.servehttp.com-0001/fullchain.pem
               SSLCertificateKeyFile /etc/letsencrypt/live/projecte-malj.servehttp.com-0001/privkey.pem
~~~

**5-** Ara habilitarem el lloc web utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2ensite projecte-malj-ssl,conf
~~~

**6-** Ara ens demanarà que fem restart del **apache**, per fer això utilitzarem la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo service apache2 restart
~~~

# MANUAL TÈCNIC DEL SERVEI FTP PASSIU

**1-** Començarem amb la instal·lació del servei FTP amb la següent comanda:

~~~
usuari@ftp:~$ sudo apt-get isntall vsftpd
~~~

**2-** Una vegada tinguem el paquet instal·lat, anirem al fitxer de configuració amb `sudo nano /etc/vsftpd.conf` i desmarquem i modifiquem els paràmetres que necessitem/vulguem. En el nostre cas han sigut els següents:

~~~
local_enable=YES
write_enable=YES
ftpd_banner=Benvinguts al FTP de Miguel Ángel López Jiménez :).
listen_ipv6=NO
listen=YES
anonymous_enable=NO
write_enable=YES
connect_from_port_20=NO
pasv_enable=YES
pasv_min_port=2000
pasv_max_port=2200
~~~

Quan acabem de configurar, reiniciarem el servei amb `sudo service vsftp restart`. Amb això ja tindríem un FTP bàsic.

### GABIES

Ara farem les gàbies, ja que ens interessa que els usuaris estiguin engabiats a la seva home, i només puguin moure's a la seva carpeta i crear, esborrar, pujar, agafar fitxers des de allà.

**1-** Per fer les bústies hem de tornar al fitxer `/etc/vsftp.conf` i desmarquem i modifiquem aquests paràmetres:

~~~
chroot_local_user=YES

chroot_list_enable=YES

chroot_list_file=/etc/vsftpd.chroot_list
~~~

**2-** Després, creem el fitxer `/etc/vsftpd.chroot_list` que és el que conté la llista dels usuaris que no estaran engabiats.
En el nostre cas només contindrà l'usuari Ubuntu, que és el root.

`sudo nano /etc/vsftpd.chroot_list`.

`sudo service vsftp restart`

### CREAR USUARI DE PROVA

Crearem un usuari per comprovar que el FTP i les gàbies funcionen correctament amb la comanda `sudo adduser prova`

I completem el que ens demana:

~~~
New password:
Retype new password:
passwd: password updated successfully
Changing the user information for prova
Enter the new value, or press ENTER for the default
	Full Name []:
	Room Number []:
	Work Phone []:
	Home Phone []:
	Other []:
Is the information correct? [Y/n]
~~~

### CANVIAR PERMISOS

**1-** Anem al directori `/home/prova`

**2-** I fem les següents comandes:

`sudo chown root .` Per fer que **root** sigui el propietari.

`sudo mkdir /home/prova/Documentacio_projecte` fem la carpeta que ell utilitzarà per treballar.

`sudo chown prova /home/prova/Documentacio_projecte` per fer propietari al **usuari** de prova a la seva carpeta de treball.

### COMPROVAR FTP BÀSIC

Si ho volem fer de forma local:

**1-** Primer executarem la comanda `sudo su`

**2-** Desprès farem: `login user_prova`

**3-** Escrivim `ftp localhost`

~~~
prova@ftp:~$ ftp localhost
Connected to localhost.
220 Benvinguts al FTP de Miguel Ángel López Jiménez :).
Name (localhost:prova):
~~~

### COMPROVAR LES GABIES

**1-** Primer iniciarem sessió amb l'usuari desitjat:

~~~
ftp prova@192.168.1.7:~> ls
drwxr-xr-x    2 1002     0            4096 Jan 30 11:14 user_prova_home

~~~

**2-** Ara provarem a pujar i descarregar arxius:

~~~
ftp prova@192.168.1.7:/> cd user_prova_home/
lftp prova@192.168.1.7:/Documentacio_projecte> ls
-rw-rw-r--    1 1002     1002            0 Jan 30 11:01 hola.txt
-rw-r--r--    1 0        0               0 Feb 03 07:37 holaaaaa.txt
-rw-------    1 1002     1002          334 Feb 03 07:33 notes_secretari_1
lftp prova@192.168.1.7:/Documentacio_projecte> get holaaaaa.txt
~~~

### FER SSL

Farem el **SSL** per el **FTP** per fer-ho més segur.

**1-** Primer anem al fitxer `/etc/vsftpd.conf` i afegim les següents línies:

~~~
rsa_cert_file=/etc/ssl/certs/vsftpdcertificate.pem
rsa_private_key_file=/etc/ssl/private/vsftpdserverkey.pem
ssl_enable=YES
allow_anon_ssl=NO
force_local_data_ssl=YES
force_local_logins_ssl=YES
ssl_tlsv1=YES
ssl_sslv2=YES
ssl_sslv3=YES
~~~

Les dues primeres línies ja venen per defecte, només haurem de canviar la ruta del fitxer de certificat i la ruta de la clau privada com està escrit a dalt.

**2-** Després creem el nostre propi certificat amb la següent comanda:

~~~
sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout/etc/ssl/nom_certificat.pem -out /etc/ssl/certs/nom_certificat.pem -days 365
~~~

Una vegada has executat aquesta comanda, et demanarà informació com pot ser a quin país vius, com et dius... Així que completem el que ens demana.

Per últim, reiniciem el servei.

### COMPROVAR EL SSL

**1-** Per comprovar que funciona el **SSL** del **FTP**, necessitem instal·lar el paquet **lftp**.
`sudo nano apt-get install lftp`

**2-** Seguidament, escrivim `lftp 192.168.1.7 (IP server) 955`  
Quan entrem a dins, escrivim `set ssl:verify-certificate no` perquè detecti el nostre **SSL** com a vàlid.  
A continuació amb `login prova` (nom usuari) i després escrivint la contrasenya entrarem amb la compte.

~~~
miguelangel@malj.pc:~$ lftp 192.168.1.7
lftp 192.168.1.7:~> set ssl:verify-certificate no
lftp 192.168.1.7:~> login prova
Password:
lftp prova@192.168.1.7:~> ls
drwxr-xr-x    2 1002     0            4096 Feb 03 07:37 user_prova_home
lftp prova@192.168.1.7:/>
~~~

### COSES A TENIR EN COMPTE

Crear els logs:

**touch /var/log/vsftpd.log**

Des-comentem aquesta línia per dir a quina ruta està els logs en el document **/etc/vsftpd.conf**:

~~~
xferlog_file=/var/log/vsftpd.log
~~~

Per comprovar de forma externa de que el **FTP** funciona correctament es connectar-nos a traves d'un FTP públic com https://www.onlineftp.ch/ o https://webftp.dreamhost.com/.

# MANUAL D'USUARI DE FTP

**NOTA:** En cas de tenir qualsevol problema amb qualsevol dels següents passos contactar amb el teu administrador.

## LINUX (UBUNTU)

**1-** Començarem amb la instal·lació de FileZilla a Linux:

Anem a la tenda d'**Ubuntu** i busquem **FileZilla**.

![](assets2/filezilla1.png)  

Prenem a sobre i després a **"Instal·lar"**

**2-** Ara obrirem el **Filezilla** i  veurem el següent:

![](assets2/manual_usuari_FTP-c12596e9.png)

**3-** Per connectar-nos, prenem a sobre d'aquest botó que està a la part esquerra superior:  

![](assets2/filezilla4.png)

**4-** Configurar el FileZilla per connectar-nos al servidor **FTP**:

![](assets2/manual_usuari_FTP-612d1ca0.png)

En el paràmetre **"Amfitrió""** escrivim el nom del servidor **FTP**, en el nostre servidor és diu **projecte-malj.servehttp.com**.

A **"Protocol"** seleccionem l'opció **"FTP"** i xifratge seleccionem **"TLS"**.

Posem el nom del nostre usuari a **"Usuari"** i la nostre contrasenya a **"Contrasenya"**.

Per últim prenem el botó de **"Connecta**, marquem l'opció **"Sempre confia en aquest amfitrió, afegeix aquesta clau a la memòria cau"**, i **"D'acord"**:

![](assets2/filezilla6.png)

Ara ja estarem connectats al servidor FTP amb el nostre usuari:

![](assets2/filezilla7.png)

**5-** Per pujar arxius, hem de navegar per les carpetes fins a arribar al fitxer que volem pujar a la part inferior esquerra, i arrossegar-la cap a la part dreta, a on està el home del nostre usuari de FTP:

![](assets2/filezilla8.png)  

![](assets2/filezilla9.png)

**6-** Per baixar un fitxer, prenem botó dret a sobre del fitxer que estigui en el FTP i prenem **"Baixar"**:

![](assets2/filezilla10.png)

![](assets2/filezilla11.png)

## WINDOWS

**1-** Busquem en el navegador **filezilla download**:

![](assets2/filezillaw1.png)

Entrem en el primer link i prenem a **Download Filezilla Client**

![](assets2/filezillaw2.png)

Seleccionem la primera opció:

![](assets2/filezillaw3.png)

**Ejecutar**:

![](assets2/filezillaw4.png)

**2-** Ara s'obrira l'insta·lador, li donem a **"Sí"**.

![](assets2/filezillaw5.png)

**3-** Ara li donem a **"I Agree"** per acceptar els terminis d'us.

![](assets2/filezillaw6.png)

**4-** Ara deixem marcada per defecte la opció que surt i li donem a **"Next"**.

![](assets2/filezillaw7.png)

**5-** Deixem les opcions que estan per defecte i li donem a **"Next"**.

![](assets2/filezillaw8.png)

**6-** Deixem la ruta per defecte d'instal·lació i li donem a **"Next"**.

![](assets2/filezillaw9.png)

**7-** Per últim li donem a **"Finish"**.

![](assets2/filezillaw10.png)

**2-** Per connectar-nos, prenem a sobre d'aquest botó que està a la part esquerra superior:

![](assets2/filezilla4.png)

**3-** Configurar el FileZilla per connectar-nos al servidor **FTP**:

![](assets2/manual_usuari_FTP-612d1ca0.png)

En el paràmetre **"Amfitrió""** escrivim el nom del servidor **FTP**, en el nostre servidor és diu **projecte-malj.servehttp.com**.

A **"Protocol"** seleccionem l'opció **"FTP"** i xifratge seleccionem **"TLS"**.

Posem el nom del nostre usuari a **"Usuari"** i la nostre contrasenya a **"Contrasenya"**.

Per últim prenem el botó de **"Connecta**, marquem l'opció **"Sempre confia en aquest amfitrió, afegeix aquesta clau a la memòria cau"**, i **"D'acord"**:

![](assets2/filezilla6.png)

Ara ja estarem connectats al servidor **FTP** amb el nostre usuari:

![](assets2/filezilla7.png)

**4-** Per pujar arxius, hem de navegar per les carpetes fins a arribar al fitxer que volem pujar a la part inferior esquerra, i arrossegar-la cap a la part dreta, a on està el home del nostre usuari de FTP:

![](assets2/filezilla8.png)  

![](assets2/filezilla9.png)

**5-** Per baixar un fitxer, prenem botó dret a sobre del fitxer que estigui en el FTP i prenem **Baixar**:

![](assets2/filezilla110.png)

![](assets2/filezilla11.png)

# MANUAL TÈCNIC DE CONFIGURACIÓ DE CORREU (POSTFIX, DOVECOT, BUSTIES, SPAMASSASSIN I SSL)

## CONFIGURACIÓ BÀSICA DE POSTFIX

**1-** Primer començarem amb la instal·lació de Postfix, per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo apt-get install postfix
ubuntu@kyle:~$ sudo apt-get install mailutils (opcional)
~~~

**2-** Ara realitzarem la configuració inicial on tenim la possibilitat de posar el nostre domini, xarxa, mida màxima de les busties dels usuaris, etc. Per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo dpkg - reconfigure postfix
~~~

### INFORMACIÓ ADDICIONAL

El directori de configuració de **Postfix** és el següent:

~~~
/etc/postfix
~~~

I els fitxers de logs de **Postfix** es poden comprovar de la següent manera:

~~~
sudo tail -f /var/log/syslog | grep postfix
sudo tail -f /var/log/mail.err
sudo tail -f /var/log/mail.log
~~~

**3-** Una vegada instal·lat **Postfix** podem realitzar una prova en local per comprovar el seu correcte funcionament, per fer això utilitzarem la següent comanda:

~~~
telnet localhost 25
~~~

Aquestes son les comandes essencials de **Postfix** (**SMTP**):


COMANDA  |  PER A QUE SERVEIX?
--|--
EHLO  |  Serveix per obrir sessió.
MAIL FROM  |  Serveix per determinar qui envia el correu.
RCPT TO  |  Serveix per determinar a qui va dirigit el correu
DATA  |  Aquesta comanda seveix per iniciar el missatge que anem a enviar, per acabar fa falta posar un "." en una línia a part.
QUIT  |  Serveix per tancar Postfix (SMTP).
RSET  |  Serveix per avortar el missatge que anàvem a enviar.
VRFY  |  Serveix per verificar que una bustia existeix
NOOP  |  Aquesta comanda força al receptor a enviar un OK.
SEND  |  Serveix per enviar els correus en cua.
HELP  |  Aquesta comanda et mostra totes les comandes possibles.

**4-** Ara podem configurar els paràmetres desitjats de **Postfix** en _**/etc/postfix/main.cf**_

DIRECTIVA  |  DESCRICPICÓ
--|--
smtpd_banner  |  Això és el missatge de benvinguda. Si trobem $ davant una paraula vol dir que és una variable i volem el valor que contingui. Per exemple $myhostname posarà el nom del nostre equip.
myhostname  |  Estableix el nom del host del servidor
alias_maps  |  Indica on està el fitxer d'àlies de correu
alias_database  |  Indica on està el fitxer de base de dades d'àlies de correu i de quin tipus es.
myorigin  |  Indica quin es el domini d'origen. El domini a afegir per defecte al compte de correu que es fa servir.
mydestination  |  Dominis que es considerin que són el destí gestionat per aquest servidor Postfix.
relayhost  |  Adreça IP del MTA al que es reenviarà el correu d'aquest MTA.
mynetworks  |  Xarxa o xarxes i/o hosts als que està permès reenviar el seu correu per aquest MTA.
mailbox_size_limit  |  Mida màxim de bústia per usuari, el 0 = a infinit.
recipient_delimeter  |  Serveix per separar els destinataris de correu.
inet_interfaces  |  Serveix per definir les interfícies per on escolta Postfix.

**5-** Una vegada realitzat els canvis desitjats reiniciem el servei:

~~~
ubuntu@kyle:~$ sudo service postfix restart
~~~

## CONFIGURACIÓ BÀSICA DE DOVECOT

**1-** Primer començarem amb la instal·lació de **Dovecot**, per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo apt-get install dovecot-impad dovecot-pop3d
~~~

**2-** Ara iniciem el servei:

~~~
ubuntu@kyle:~$ sudo service dovecot restart
~~~

**2-** Una vegada instal·lat no fa falta res més per que funcioni amb les funcions més bàsiques, farem un test per comprovar que funciona correctament, podem fer-ho tant amb **POP3** o **IMAP**:

### COMPROVACIÓ AMB POP3

~~~
ubuntu@kyle:~$ telnet localhost pop3
~~~

### COMANDES DE POP3

COMANDA  |  DESCRIPCIÓ
--|--
USER nom_usuari  |  Serveix per indicar el nom d'usuari amb el qual volem iniciar sessió.
PASS password  |  Serveix per indicar la contrasenya del usuari amb el qual volem iniciar sessió.
STAT  |  Serveix per veure l'estat del nostre correu.
LIST  |  Serveix per veure quant correus tenim en la bústia.
RETR num_message  |  Serveix per indicar quin missatge volem llegir.
TOP num_message num_linies  |  Serveix per indicar quin missatge volem veure i quantes línies volem veure.
DELE num_message  |  Serveix per indicar quin missatge volem esborrar.
RSET  |  Serveix per recuperar els missatges esborrats.
UIDL  |  Mostra els números dels missatges.
QUIT  |  Serveix per tancar la connexió de POP3.

### COMPROVACIÓ AMB IMAP

~~~
ubuntu@kyle:~$ telnet localhost imap
~~~

COMANDA  |  DESCRIPCIÓ
--|--
LOGIN [username] [password]  |  Serveix per iniciar sessió amb l'usuari desitjat.
LIST [flags] [folder separator] [search term]  |  Serveix per llistar tots els correus.
STATUS [mailbox] [flags]  |  Serveix per veure l'estat de la bustia de correu.
SELECT [mailbox]  |  Serveix per seleccionar una bustia de correu.
FETCH [first]:[last] [flags]  | Serveix per seleccionar uns missatges de cop.
FETCH [mail number] body[header]  |  Serveix per seleccionar un missatge i el seu títol.
FETCH  [mail number]  body[text]  |  Serveix per seleccionar un missatge i el seu cos.
LOGOUT  |  Serveix per tancar la connexió de IMAP.

## CONFIGURACIÓ DE LES BÚSTIES MAILDIR

**1-** El que tenim que fer primer de tot es crear la carpeta **Maildir** en el home dels usuaris existents:

~~~
ubuntu@kyle:~$ mkdir /home/ubuntu/Maildir
~~~

**2-** Per a que els directoris es generin de manera automàtica al crear un usuari primer tenim que afegir la següent variable al arxiu **/etc/profile** o **/etc/skel/.profile**:

~~~
MAIL=/home/$USER/Maildir o MAIL=/$HOME/Maildir
~~~

**3-** Ara canviarem la bústia que utilitza **Dovecot**, per fer això tenim que anar a l'arxiu _**/etc/dovecot/conf.d/10-mail.conf**_ i afegir el següent:

### PER A MAILDIR

~~~
mail_location = maildir:~/Maildir
~~~

**NOTA:** Per defecte ve habilitat **Mailbox**, en cas de voler utilitzar **Maildir** documentar la línia de **Mailbox**.

### PER A MBOX

~~~
mail_location = mbox :~/ mail : INBOX =/ var / spool / mail /% u
~~~

**4-** Per últim reiniciem el servei per aplicar els canvis:

~~~
ubuntu@kyle:~$ sudo service dovecot restart
~~~

## CONFIGURACIÓ DE SPAMASSASSIN

**1-** Primer començarem amb la instal·lació de **Spamassassin**, per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo apt-get install spamassasin spamc
~~~

**2-** Ara afegirem l'usuari **spamd** utilitzant la següent comanda:

~~~
ubuntu@kyle:~$ adduser spamd --disabled -login
~~~

**3-** Ara començarem amb la configuració de **spamassassin**, primer farem que s'actualitzi automàticament, per fer això anirem a _**/etc/default/spamassasin**_ i canviarem el següent paràmetre a 1:

~~~
CRON =1
~~~

**4-** Ara en el mateix arxiu definirem on es troba la carpeta de logs:

~~~
SAHOME ="/var/log/spamassassin/"
~~~

**5-** Ara arrancarem **spamassasin** amb la següent comanda:

~~~
ubuntu@kyle:~$ sudo service spamassassin start
~~~

**5-** Ara configurarem **Postfix** per a que utilitzi **spamassassin**, per fer això anirem al arxiu _**/etc/postfix/master.cf**_ i afegirem o modificarem aquestes línies:

### AL INICI DEL ARXIU

~~~
smtp inet n - - - - smtpd -o content_filter = spamassassin
smtps inet n - y - - smtpd -o content_filter = spamassassin
~~~

### AL FINAL DEL ARXIU

~~~
spamassassin unix - n n - - pipe user=spamd argv=/usr/bin/spamc -f -e /usr/sbin/sendmail -oi -f ${sender} ${recipient}
~~~

**6-** Si volem que **spamassasin** marqui els correus amb la puntuació d'spam anirem al arxiu _**/etc/spamassassin/local.cf**_ i deixarem el següent paràmetre així:

~~~
rewrite_header Subject ***** SPAM _SCORE_ *****
~~~

En cas de voler canviar l'score canviarem el següent paràmetre:

~~~
required_score [valor] // per exemple 3.0
~~~

**7-** Ara per aplicar tots els canvis realitzats reiniciarem el servei:

~~~
ubuntu@kyle:~$ sudo service spamassasin restart
~~~

**8-** Per últim per comprovar que **spamassassin** funciona correctament enviarem un correu amb el **gtube**, que es un test per comprovar el funcionament d'**spamassassin**:

~~~
XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X
~~~

## CONFIGURAR SSL PER A POSTFIX I DOVECOT

**1-** Primer tenim que generar el certificat i la clau que utilitzaran tant **Postfix** com **Dovecot**, per fer això executarem les següents comandes:

### GENERAR LA CLAU I LA PETICIÓ DE FIRMA DEL CERTIFICAT

~~~
ubuntu@kyle:~$ openssl req -new -newkey rsa:2048 -nodes -keyout nom_clau.key -out nom_certificat.csr
~~~

### GENERAR EL CERTIFICAT

~~~
ubuntu@kyle:~$ openssl x509 -req -days 365 -in nom_certificat.csr -signkey nom_clau.key -out nom_certificat.crt
~~~

**2-** Ara mourem tant la clau i el certificat al directori corresponent:

~~~
ubuntu@kyle:~$ cp nom_clau.key /etc/ssl/private
ubuntu@kyle:~$ cp nom_certificat.crt /etc/ssl/certs
~~~

### CONFIGURAR SSL PER A POSTFIX

**1-** Primer anirem al arxiu _**/etc/postfix/main.cf**_ i afegirem les següents línies:

~~~
# TLS parameters
smtpd_tls_cert_file=/etc/ssl/certs/nom_certificat.crt
smtpd_tls_key_file=/etc/ssl/private/nom_clau.key
smtpd_use_tls=yes
smtpd_tls_auth_only = yes
smtpd_tls_security_level = may
smtp_tls_security_level = may
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
smtpd_sasl_security_options = noanonymous, noplaintext
smtpd_sasl_tls_security_options = noanonymous
~~~

**2-** Ara anirem al arxiu _**/etc/postfix/master.cf**_ i afegirem el següent:

~~~
submission inet n       -       n       -       -       smtpd -o content_filter=spamassassin
  -o syslog_name=postfix/submission
  -o smtpd_tls_security_level=encrypt
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_sasl_type=dovecot
  -o smtpd_sasl_path=private/auth
  -o smtpd_reject_unlisted_recipient=no
  -o smtpd_client_restrictions=permit_sasl_authenticated,reject
  -o milter_macro_daemon_name=ORIGINATING

smtps     inet  n       -       n       -       -       smtpd -o content_filter=spamassassin
    -o syslog_name=postfix/smtps
    -o smtpd_tls_wrappermode=yes
    -o smtpd_sasl_auth_enable=yes
    -o smtpd_sasl_type=dovecot
    -o smtpd_sasl_path=private/auth
    -o smtpd_client_restrictions=permit_sasl_authenticated,reject
    -o milter_macro_daemon_name=ORIGINATING
~~~

### CONFIGURAR LA SSL PER A DOVECOT

**1-** Primer anirem a l'arxiu _**/etc/dovecot/conf.d/10-ssl.conf**_ i canviarem la línia **SSL** i la deixarem així:

~~~
ssl = required
~~~

**2-** En el mateix arxiu posarem les rutes dels certificats que volem utilitzar:

~~~
ssl_cert = </etc/ssl/certs/nom_certificat.crt
ssl_key = </etc/ssl/private/nom_clau.key
~~~

**3-** Ara anirem a l'arxiu _**/etc/dovecot/conf.d/10-auth.conf**_ i buscarem el següent paràmetre i l'habilitarem per deshabilitar l'autenticació de text pla:

~~~
disable_plaintext_auth = yes
~~~

**4-** Ara anirem al arxiu _**/etc/dovecot/conf.d/10-master.conf**_ i afegirem i/o modificarem els següents paràmetres:

~~~
service imap-login {
  inet_listener imap {
    port = 143
  }
  inet_listener imaps {
    port = 993
    ssl = yes
  }

  service pop3-login {
    inet_listener pop3 {
      port = 110
    }
    inet_listener pop3s {
      port = 995
      ssl = yes
    }
  }

  unix_listener auth-userdb {
      mode = 0666
      user = postfix
      group = postfix
    }

    unix_listener /var/spool/postfix/private/auth {
        mode = 0666
      }
~~~

**5-** Ara reiniciem els serveis per aplicar els canvis:

~~~
ubuntu@kyle:~$ sudo service postfix restart
ubuntu@kyle:~$ sudo service dovecot restart
~~~

**6-** Ara comprovarem a enviar un correu per el port segur:

~~~
ubuntu@kyle:~$ telnet localhost 465
~~~

**7-** Una vegada comprovat que podem enviar correus per el port segur comprovarem que podem llegir els correus per el port segur, tant en **POP3S** i **IMAPS**:

### COMPROVACIÓ SSL POP3S

~~~
ubuntu@kyle:~$ telnet locahost 995
~~~

### COMPROVACIÓ SSL IMAPS

~~~
ubuntu@kyle:~$ telnet localhost 993
~~~

# MANUAL TÈCNIC DE INSTAL·LACIÓ/CONFIGURACIÓ DE NAGIOS

## INSTAL·LACIÓ DE NAGIOS

**1-** Per a instal·lar **NagiosIX** ho podem fer de fues formes, manual o automàtica.

Per fer-lo de manera automàtica executarem la següent comanda:

~~~
usuari@server:~$ curl https://assets.nagios.com/downloads/nagiosxi/install.sh | sudo sh
~~~

Per fer-ho de manera manual executarem les següents comandes:

~~~
cd /tmp
wget https://assets.nagios.com/downloads/nagiosxi/xi-latest.tar.gztar
xzf xi-latest.tar.gz
cd nagiosxi
./fullinstall
~~~

**2-** Ara configurarem el **Nagios** a traves de la interfície web, per fer això obrim el navegador e introduïm la direcció IP del nostre servidor. Una vegada dins ens sortirà aquesta finestra que ens permetrà accedir al **Nagios**.

![](assets/monitoratge-xarxes-malj-d85b8251.png)

**3-** Ara ens preguntarà per la direcció URL que volem que tingui per accedir al **Nagios**, la zona horària, el llenguatge i el mode de la interfície gràfica. Una vegada fet això li donem a **"Next"** per continuar.

![](assets/monitoratge-xarxes-malj-e54edc9c.png)

**4-** Ara ens preguntarà per la contrasenya, el nom complert del usuari administrador i per una direcció de correu electrònic. Una vegada fet això li donem a **"Finish install"** per acabar la instasl·lació.

![](assets/monitoratge-xarxes-malj-6ad5fb38.png)

## CONFIGURAR UNA TASCA D'AUTO DESCOBRIMENT

**1-** Per crear una tasca d'auto descobriment primer tenim que anar a **"Configurar"** -> **"Tareas de Auto-descubrimiento"**.

![](assets/monitoratge-xarxes-malj-28851d8e.png)

**2-** Ara li donem a **"+ Tareas de Auto-descubrimiento"**.

![](assets/monitoratge-xarxes-malj-af6e777c.png)

**3-** Ara configurarem la nova tasca.

![](assets/monitoratge-xarxes-malj-b94b588f.png)

**4-** Una vegada acaba d'executar la tasca podem veure els dispositius que a trobats donant-li a **"Nuevo"**.

![](assets/monitoratge-xarxes-malj-4c085136.png)

**5-** Ara li donem a **"Proximo"** per a continuar.

![](assets/monitoratge-xarxes-malj-d3c5ea47.png)

**6-** Aquí veiem un resum dels dispositius trobats i els serveis que tenen habilitats. Li donem a **"Next"**.

![](assets/monitoratge-xarxes-malj-2e3ec971.png)

**7-** Aquí podem seleccionar el temps que volem que trigui a comprovar l'estat dels dispositius. Fet això li donem a **"Finalizar"**.

![](assets/monitoratge-xarxes-malj-2d5a30fa.png)

**8-** Per veure els dispositius connectats i els serveis que tenen tenim que anar a **"Vistas"**.

**SERVEIS**

![](assets/monitoratge-xarxes-malj-7cead54f.png)

![](assets/monitoratge-xarxes-malj-9efb4b60.png)

![](assets/monitoratge-xarxes-malj-c1aed303.png)

**DISPOSITIUS**

![](assets/monitoratge-xarxes-malj-8a1bf579.png)

# MANUAL TÈCNIC DE OPENVPN EN PFSENSE

## CONFIGURACIÓ DE SERVIDOR OPENVPN

**1-** Primer tenim que anar a **"VPN"** -> **"OpenVPN"**.

![](assets/openvpn-malj-e228465c.png)

**2-** Ara anirem a la finestra de **"Wizards"**.

![](assets/openvpn-malj-80c196d9.png)

**3-** Ara li donarem a **"Next"** per a començar la configuració.

![](assets/openvpn-malj-64773190.png)

**4-** Ara crearem la **CA (Certificate Authority)** que utilitzarem.

![](assets/openvpn-malj-03e3b48d.png)

**5-** Ara crearem el certificat per el servidor.

![](assets/openvpn-malj-192b8f88.png)

**6-** En aquesta part de la configuració ho deixarem com està per defecte. Es molt important que el port sigui el 1194 i que el protocol sigui **UDP**.

![](assets/openvpn-malj-9a91c693.png)

**7-** Aquest apartat ho deixarem tal i com surt per defecte.

![](assets/openvpn-malj-3699f0b6.png)

**8-** Ara indicarem quina serà la xarxa que utilitzarà la **VPN** i quina és la xarxa local.

![](assets/openvpn-malj-3e069db0.png)

**9-** Ara li direm que utilitzi direccions IPs dinàmiques.

![](assets/openvpn-malj-41916121.png)

**10-** Una vegada fet això baixem al final i li donem a **"Next"**.

![](assets/openvpn-malj-826350a6.png)

**11-** Ara marquem les caselles de **"Firewall Rule"** per afegir a la **WAN** la regla que permet el trafic de la **VPN** i la de **"OpenVPN rule"** per afegir la interfície de la **VPN**.

![](assets/openvpn-malj-e0171480.png)

**12-** Per últim li donem a **"Finish"**.

![](assets/openvpn-malj-584f27a2.png)

**13-** Ara crearem l'usuari que utilitzara per la **VPN**, per fer això anirem a **"System"** -> **"User Manager"**.

![](assets/openvpn-malj-b0042b13.png)

**14-** Ara introduïm les dades del nostre usuari.

![](assets/openvpn-malj-3d70afae.png)

**15-** Ara baixem i marquem la casella de **"Certificate"** i omplim les dades i li donem a **"Save"**.

![](assets/openvpn-malj-792e59aa.png)

**16-** Ara descarregarem el modul que permet exportar el certificat al nostre client. Per fer això tenim que anar a **"System"** -> **"Package Manager"**.

![](assets/openvpn-malj-1cb66d87.png)

**17-** Ara anirem a **"Avaiable packages"** i busquem el paquet **"openvpn-client-export"** i li donem a **"Install"**.

![](assets/openvpn-malj-41d79575.png)

**18-** A continuació anirem a **"VPN"** -> **"OpenVPN"**.

![](assets/openvpn-malj-e228465c.png)

**19-** Ara anem a **"Client Export"**.

![](assets/openvpn-malj-a123481d.png)

**20-** Marcarem la casella de **"Legacy Client"**.

![](assets/openvpn-malj-8cef8784.png)

**21-** Ara anem a l'apartat **"OpenVPN Clients"** i en **"Inline Clients"** li donem a **"Most Clients"** per descarregar el certificat i amb aquest poder-nos connectar a la VPN.

![](assets/openvpn-malj-2120cc33.png)

## CONNECTAR-SE A LA VPN EN UBUNTU

**NOTA:** Per poder realitzar els següents passos necessitaràs que el teu administrador t'hagui proporcionat l'arxiu de configuració per a la **VPN** i en cas de tenir qualsevol problema seguint els següents passos contactar amb el teu administrador per solucionar-los..

**1-** instal·larem el paquet _**network-manager-openvpn-gnome**_ utilitzant la següent comanda:

~~~
sudo apt-get install network-manager-openvpn-gnome
~~~

**2-** Ara a la icona de **"Network Manager"** -> **"VPN"** -> símbol **"+"** (afegir)-> **"Importa des d'un fitxer"**.

![](assets/openvpn-malj-8ca8a05b.png)

**3-** Ara per configurar uns paràmetres, per fer això li donarem a **"Avançat"**.

![](assets/openvpn-malj-9d3265d5.png)

**4-** En la finestra **"General"** marcarem la casella que surt a la imatge.

![](assets/openvpn-malj-0d375f03.png)

**5-** En la finestra de **"Seguretat"** ho deixarem com surt en la imatge. Fet això li donem a **"D'acord"**.

![](assets/openvpn-malj-e6517514.png)

## CONNECTAR-SE A LA VPN EN WINDOWS

**NOTA:** Abans d'intentar connectar-se a la **VPN** des de Windows crearem un nou usuari i un nou certificat per aquest.

**USUARI MLOPEZ2**

![](assets/openvpn-malj-9ce053a5.png)

**CERTIFICAT USUARI MLOPEZ2**

![](assets/openvpn-malj-f586f808.png)

Windows de forma nativa suporta els següents tipus de VPN:

- Protocolo túnel punto a punto (PPTP)
- L2TP/IPsec con certificado
- L2TP/IPsec con clave previamente compartida
- Protocolo de túnel de sockets seguros (SSTP)
- IKEv2

**NOTA:** Per Windows tenim dos programes el tradicional de **"OpenVPN"** o **"OpenVPN Connect"**, ela segona opció és més amigable amb l'usuari però en cas de no funcionar utilitzar la primera opció.

**INSTAL·LACIÓ DEL SOFTWARE OPENVPN EN WINDOWS 10**

**1-** Primer anirem a la pàgina https://openvpn.net/community-downloads/ i descarregarem el paquet corresponent a **Windows 10**.

![](assets/openvpn-malj-db1c3048.png)

**2-** Ara descarreguem l'instal·lador del certificat de **Pfsense** per a Windows 10.

![](assets/openvpn-malj-75d04aa6.png)

**3-** Una vegada descarregat l'instal·lem.

![](assets/openvpn-malj-4b2a4840.png)

**4-** Ara fem click dret a la icona de la aplicació de **OpenVPN** i li donem a **"Conectar"**.

![](assets/openvpn-malj-199d7856.png)

**5-** Per últim introduïm les credencials i li donem a **"OK"**.

![](assets/openvpn-malj-ec98bb70.png)

**INSTAL·LACIÓ DEL SOFTWARE OPENVPN CONNECT EN WINDOWS 10**

**1-** Anem al següent enllaç i descarregem la aplicació "OpenVPN Connect" https://openvpn.net/client-connect-vpn-for-windows/, li donem a "DOWNLOAD OPENVPN CONNECT"

![](assets/manual_usuari_openvpn-abda54b2.png)

**2-** Una vegada obert el programa d'instal·lació que acabem de descarregar s'obrira una finestra, li donem a "Next".

![](assets/manual_usuari_openvpn-c6da1438.png)

**3-** Ara ens demanarà acceptar els terminis d'us. Marquem la casella i li donem a **"Next"**.

![](assets/manual_usuari_openvpn-dba3078a.png)

**4-** Per últim li donem a **"Install"**.

![](assets/manual_usuari_openvpn-cb90568d.png)

**5-** Ara obirm el programa i ens situem sobre l'apartat **"FILE"** i li donem a **"BROWSE"** per buscar l'arxiu de configuració que ens ha sigut proporcionat..

![](assets/manual_usuari_openvpn-1ef87cda.png)

**6-** Una vegada trobat l'arxiu introduïm l'usuari i contrasenya que ens a sigut proporcionats, marquem la casella **"Connect after import"** per connectar-nos quan afegim la **VPN** i finalment li donam a **"Add"**.

![](assets/manual_usuari_openvpn-38876904.png)

**7-** Una vegada connectats en aquesta finestra podem veure tota la informació del trafic que estem generant a la **VPN**.

![](assets/manual_usuari_openvpn-ddfeaa37.png)
