# MANUAL TÈCNIC DE CONFIGURACIÓ DEL ROUTER LOWI

## COM ACCEDIR AL ROUTER

Per accedir al router tenim que posar la IP del nostre router al navegador web.

![](assets/manual_tecnic_router_lowi-2121150c.png)

## REENVIAMENT DE PORTS

Accedim al router ens posem en **"Modo Avanzado"** d'usuari anem a la finestra de **"Internet"** -> **"Redirección de Puertos"** i li donem al botó **"+"** per afegir un de nou i omplim les dades que ens demana. Una vegada fet per guardar els canvis li donem a **"Aplicar"**.

![](assets/manual_tecnic_router_lowi-27c65be8.png)

### TAULA DE TOTS ELS RENVIAMENTS DEL ROUTER

![](assets/manual_tecnic_router_lowi-018ab4a6.png)

## CONFIGURACIÓ DDNS

Anem a **"DNS & DDNS"** i en **"Proveedor"** seleccionem **"no-ip.com"** i omplim les dades que ens demana. Fet això li donem a **"Aplicar"**.

![](assets/manual_tecnic_router_lowi-ff49fd57.png)
