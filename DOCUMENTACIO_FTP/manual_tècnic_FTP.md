# MANUAL TÈCNIC DEL SERVEI FTP PASSIU

**1-** Començarem amb la instal·lació del servei FTP amb la següent comanda:

~~~
usuari@ftp:~$ sudo apt-get isntall vsftpd
~~~

**2-** Una vegada tinguem el paquet instal·lat, anirem al fitxer de configuració amb `sudo nano /etc/vsftpd.conf` i desmarquem i modifiquem els paràmetres que necessitem/vulguem. En el nostre cas han sigut els següents:

~~~
local_enable=YES
write_enable=YES
ftpd_banner=Benvinguts al FTP de Miguel Ángel López Jiménez :).
listen_ipv6=NO
listen=YES
anonymous_enable=NO
write_enable=YES
connect_from_port_20=NO
pasv_enable=YES
pasv_min_port=2000
pasv_max_port=2200
~~~

Quan acabem de configurar, reiniciarem el servei amb `sudo service vsftp restart`. Amb això ja tindríem un FTP bàsic.

### GABIES

Ara farem les gàbies, ja que ens interessa que els usuaris estiguin engabiats a la seva home, i només puguin moure's a la seva carpeta i crear, esborrar, pujar, agafar fitxers des de allà.

**1-** Per fer les bústies hem de tornar al fitxer `/etc/vsftp.conf` i desmarquem i modifiquem aquests paràmetres:

~~~
chroot_local_user=YES

chroot_list_enable=YES

chroot_list_file=/etc/vsftpd.chroot_list
~~~

**2-** Després, creem el fitxer `/etc/vsftpd.chroot_list` que és el que conté la llista dels usuaris que no estaran engabiats.
En el nostre cas només contindrà l'usuari Ubuntu, que és el root.

`sudo nano /etc/vsftpd.chroot_list`.

`sudo service vsftp restart`

### CREAR USUARI DE PROVA

Crearem un usuari per comprovar que el FTP i les gàbies funcionen correctament amb la comanda `sudo adduser prova`

I completem el que ens demana:

~~~
New password:
Retype new password:
passwd: password updated successfully
Changing the user information for prova
Enter the new value, or press ENTER for the default
	Full Name []:
	Room Number []:
	Work Phone []:
	Home Phone []:
	Other []:
Is the information correct? [Y/n]
~~~

### CANVIAR PERMISOS

**1-** Anem al directori `/home/prova`

**2-** I fem les següents comandes:

`sudo chown root .` Per fer que **root** sigui el propietari.

`sudo mkdir /home/prova/Documentacio_projecte` fem la carpeta que ell utilitzarà per treballar.

`sudo chown prova /home/prova/Documentacio_projecte` per fer propietari al **usuari** de prova a la seva carpeta de treball.

### COMPROVAR FTP BÀSIC

Si ho volem fer de forma local:

**1-** Primer executarem la comanda `sudo su`

**2-** Desprès farem: `login user_prova`

**3-** Escrivim `ftp localhost`

~~~
prova@ftp:~$ ftp localhost
Connected to localhost.
220 Benvinguts al FTP de Miguel Ángel López Jiménez :).
Name (localhost:prova):
~~~

### COMPROVAR LES GABIES

**1-** Primer iniciarem sessió amb l'usuari desitjat:

~~~
ftp prova@192.168.1.7:~> ls
drwxr-xr-x    2 1002     0            4096 Jan 30 11:14 user_prova_home

~~~

**2-** Ara provarem a pujar i descarregar arxius:

~~~
ftp prova@192.168.1.7:/> cd user_prova_home/
lftp prova@192.168.1.7:/Documentacio_projecte> ls
-rw-rw-r--    1 1002     1002            0 Jan 30 11:01 hola.txt
-rw-r--r--    1 0        0               0 Feb 03 07:37 holaaaaa.txt
-rw-------    1 1002     1002          334 Feb 03 07:33 notes_secretari_1
lftp prova@192.168.1.7:/Documentacio_projecte> get holaaaaa.txt
~~~

### FER SSL

Farem el **SSL** per el **FTP** per fer-ho més segur.

**1-** Primer anem al fitxer `/etc/vsftpd.conf` i afegim les següents línies:

~~~
rsa_cert_file=/etc/ssl/certs/vsftpdcertificate.pem
rsa_private_key_file=/etc/ssl/private/vsftpdserverkey.pem
ssl_enable=YES
allow_anon_ssl=NO
force_local_data_ssl=YES
force_local_logins_ssl=YES
ssl_tlsv1=YES
ssl_sslv2=YES
ssl_sslv3=YES
~~~

Les dues primeres línies ja venen per defecte, només haurem de canviar la ruta del fitxer de certificat i la ruta de la clau privada com està escrit a dalt.

**2-** Després creem el nostre propi certificat amb la següent comanda:

~~~
sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout/etc/ssl/nom_certificat.pem -out /etc/ssl/certs/nom_certificat.pem -days 365
~~~

Una vegada has executat aquesta comanda, et demanarà informació com pot ser a quin país vius, com et dius... Així que completem el que ens demana.

Per últim, reiniciem el servei.

### COMPROVAR EL SSL

**1-** Per comprovar que funciona el **SSL** del **FTP**, necessitem instal·lar el paquet **lftp**.
`sudo nano apt-get install lftp`

**2-** Seguidament, escrivim `lftp 192.168.1.7 (IP server) 955`  
Quan entrem a dins, escrivim `set ssl:verify-certificate no` perquè detecti el nostre **SSL** com a vàlid.  
A continuació amb `login prova` (nom usuari) i després escrivint la contrasenya entrarem amb la compte.

~~~
miguelangel@malj.pc:~$ lftp 192.168.1.7
lftp 192.168.1.7:~> set ssl:verify-certificate no
lftp 192.168.1.7:~> login prova
Password:
lftp prova@192.168.1.7:~> ls
drwxr-xr-x    2 1002     0            4096 Feb 03 07:37 user_prova_home
lftp prova@192.168.1.7:/>
~~~

### COSES A TENIR EN COMPTE

Crear els logs:

**touch /var/log/vsftpd.log**

Des-comentem aquesta línia per dir a quina ruta està els logs en el document **/etc/vsftpd.conf**:

~~~
xferlog_file=/var/log/vsftpd.log
~~~

Per comprovar de forma externa de que el **FTP** funciona correctament es connectar-nos a traves d'un FTP públic com https://www.onlineftp.ch/ o https://webftp.dreamhost.com/.
