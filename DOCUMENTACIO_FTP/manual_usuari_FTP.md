# MANUAL D'USUARI DE FTP

**NOTA:** En cas de tenir qualsevol problema amb qualsevol dels següents passos contactar amb el teu administrador.

## LINUX (UBUNTU)

**1-** Començarem amb la instal·lació de FileZilla a Linux:

Anem a la tenda d'**Ubuntu** i busquem **FileZilla**.

![](assets2/filezilla1.png)  

Prenem a sobre i després a **"Instal·lar"**

**2-** Ara obrirem el **Filezilla** i  veurem el següent:

![](assets2/manual_usuari_FTP-c12596e9.png)

**3-** Per connectar-nos, prenem a sobre d'aquest botó que està a la part esquerra superior:  

![](assets2/filezilla4.png)

**4-** Configurar el FileZilla per connectar-nos al servidor **FTP**:

![](assets2/manual_usuari_FTP-612d1ca0.png)

En el paràmetre **"Amfitrió""** escrivim el nom del servidor **FTP**, en el nostre servidor és diu **projecte-malj.servehttp.com**.

A **"Protocol"** seleccionem l'opció **"FTP"** i xifratge seleccionem **"TLS"**.

Posem el nom del nostre usuari a **"Usuari"** i la nostre contrasenya a **"Contrasenya"**.

Per últim prenem el botó de **"Connecta**, marquem l'opció **"Sempre confia en aquest amfitrió, afegeix aquesta clau a la memòria cau"**, i **"D'acord"**:

![](assets2/filezilla6.png)

Ara ja estarem connectats al servidor FTP amb el nostre usuari:

![](assets2/filezilla7.png)

**5-** Per pujar arxius, hem de navegar per les carpetes fins a arribar al fitxer que volem pujar a la part inferior esquerra, i arrossegar-la cap a la part dreta, a on està el home del nostre usuari de FTP:

![](assets2/filezilla8.png)  

![](assets2/filezilla9.png)

**6-** Per baixar un fitxer, prenem botó dret a sobre del fitxer que estigui en el FTP i prenem **"Baixar"**:

![](assets2/filezilla10.png)

![](assets2/filezilla11.png)

## WINDOWS

**1-** Busquem en el navegador **filezilla download**:

![](assets2/filezillaw1.png)

Entrem en el primer link i prenem a **Download Filezilla Client**

![](assets2/filezillaw2.png)

Seleccionem la primera opció:

![](assets2/filezillaw3.png)

**Ejecutar**:

![](assets2/filezillaw4.png)

**2-** Ara s'obrira l'insta·lador, li donem a **"Sí"**.

![](assets2/filezillaw5.png)

**3-** Ara li donem a **"I Agree"** per acceptar els terminis d'us.

![](assets2/filezillaw6.png)

**4-** Ara deixem marcada per defecte la opció que surt i li donem a **"Next"**.

![](assets2/filezillaw7.png)

**5-** Deixem les opcions que estan per defecte i li donem a **"Next"**.

![](assets2/filezillaw8.png)

**6-** Deixem la ruta per defecte d'instal·lació i li donem a **"Next"**.

![](assets2/filezillaw9.png)

**7-** Per últim li donem a **"Finish"**.

![](assets2/filezillaw10.png)

**2-** Per connectar-nos, prenem a sobre d'aquest botó que està a la part esquerra superior:

![](assets2/filezilla4.png)

**3-** Configurar el FileZilla per connectar-nos al servidor **FTP**:

![](assets2/manual_usuari_FTP-612d1ca0.png)

En el paràmetre **"Amfitrió""** escrivim el nom del servidor **FTP**, en el nostre servidor és diu **projecte-malj.servehttp.com**.

A **"Protocol"** seleccionem l'opció **"FTP"** i xifratge seleccionem **"TLS"**.

Posem el nom del nostre usuari a **"Usuari"** i la nostre contrasenya a **"Contrasenya"**.

Per últim prenem el botó de **"Connecta**, marquem l'opció **"Sempre confia en aquest amfitrió, afegeix aquesta clau a la memòria cau"**, i **"D'acord"**:

![](assets2/filezilla6.png)

Ara ja estarem connectats al servidor **FTP** amb el nostre usuari:

![](assets2/filezilla7.png)

**4-** Per pujar arxius, hem de navegar per les carpetes fins a arribar al fitxer que volem pujar a la part inferior esquerra, i arrossegar-la cap a la part dreta, a on està el home del nostre usuari de FTP:

![](assets2/filezilla8.png)  

![](assets2/filezilla9.png)

**5-** Per baixar un fitxer, prenem botó dret a sobre del fitxer que estigui en el FTP i prenem **Baixar**:

![](assets2/filezilla110.png)

![](assets2/filezilla11.png)
