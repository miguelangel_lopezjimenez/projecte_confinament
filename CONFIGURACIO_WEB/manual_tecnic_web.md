# MANUAL TÈCNIC WEB

## CREACIÓ DEL VIRTUALHOST HTTP

**1-** Primer tenim que instal·lar el servei web **"apache2"**, per fer això utilitzarem la següent comanda:

~~~
usuari@phoenix:~$ sudo apt-get install apache2
~~~

**2-** Una vegada instal·lat crearem el virtual host **HTTP (80)** que redirigirà al **HTTPS (443)**. Per fer això primer tenim que anar al directori _**/etc/apache2/sites-available/**_:

~~~
usuari@phoenix:~$ cd /etc/apache2/sites-available/
~~~

**3-** Una vegada en el directori copiarem l'arxiu **000-default.conf** i li posarem el nom que vulguem, en el meu cas es diu **projecte-malj.conf**:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo cp 000-default.conf projecte-malj.conf
~~~

**4-** Ara obrirem l'arxiu que acabem de crear i l'editarem:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo nano projecte-malj.conf
~~~

**5-** Una vegada dintre afegirem el següent:

~~~
ServerName projecte-malj.servehttp.com

<IfModule mod_rewrite.c>
           RewriteEngine On
           RewriteCond %{HTTPS} off
           RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
        </IfModule>
~~~

**6-** Ara guardem els canvis i sortim, ara habilitarem el modul **"rewrite"** utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2enmod rewrite
~~~

**7-** Ara habilitarem el lloc web utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2ensite projecte-malj,conf
~~~

**8-** Ara ens demanarà que fem restart del **apache**, per fer això utilitzarem la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo service apache2 restart
~~~

## CREACIÓ DE CERTIFICATS VERIFICATS PER UNA CA UTILITZANT CERBOT

**1-** Per instal·lar **"Certbot"** primer afegirem el seu repositori executant les següents comandes:

~~~
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
~~~

**2-** Una vegada afegits el repositori utilitzarem la següent comanda per instal·lar-lo:

~~~
sudo apt-get install certbot python-certbot-apache
~~~

**3-** Ara crearem el nostre certificat i clau que utilitzarem en el següent pas, per fer això utilitzarem la següent comanda:

~~~
sudo certbot certonly --apache
~~~

**NOTA:** Al finalitzar la creació del certificat **"Certbot"** ens donarà les rutes on estan guardats els certificats, estan emmagatzemats a _**/etc/letsencrypt/live/**_.

## CREACIÓ DEL VIRTUALHOST HTTPS

**1-** Primer habilitarem el modul **SSL** de **apache** utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2enmod ssl
~~~

**2-** Ens situem en el mateix directori que amb el altre virtualhost copiarem l'arxiu **default-ssl.conf** i li posarem el nom que vulguem, en el meu cas es diu **projecte-malj-ssl.conf**:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo cp default-ssl.conf projecte-malj-ssl.conf
~~~

**3-** Ara obrirem l'arxiu que acabem de crear i l'editarem:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo nano projecte-malj-ssl.conf
~~~

**4-** Una vegada dintre afegirem el següent:

~~~
<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                ServerName projecte-malj.servehttp.com
                ServerAdmin webmaster@localhost

                DocumentRoot /var/www/html

                # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
                # error, crit, alert, emerg.
                # It is also possible to configure the loglevel for particular
                # modules, e.g.
                #LogLevel info ssl:warn

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

                Header always set Strict-Transport-Security "max-age=63072000;include Subdomains;"

                # For most configuration files from conf-available/, which are
                # enabled or disabled at a global level, it is possible to
                # include a line for only one particular virtual host. For example the
                # following line enables the CGI configuration for this host only
                # after it has been globally disabled with "a2disconf".
                #Include conf-available/serve-cgi-bin.conf

                #   SSL Engine Switch:
                #   Enable/Disable SSL for this virtual host.
                SSLEngine on

                #   A self-signed (snakeoil) certificate can be created by installing
                #   the ssl-cert package. See
                #   /usr/share/doc/apache2/README.Debian.gz for more info.
                #   If both key and certificate are stored in the same file, only the
                #   SSLCertificateFile directive is needed.
                SSLCertificateFile      /etc/letsencrypt/live/projecte-malj.servehttp.com-0001/fullchain.pem
                SSLCertificateKeyFile /etc/letsencrypt/live/projecte-malj.servehttp.com-0001/privkey.pem
~~~

**5-** Ara habilitarem el lloc web utilitzant la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo a2ensite projecte-malj-ssl,conf
~~~

**6-** Ara ens demanarà que fem restart del **apache**, per fer això utilitzarem la següent comanda:

~~~
usuari@phoenix:/etc/apache2/sites-available$ sudo service apache2 restart
~~~
