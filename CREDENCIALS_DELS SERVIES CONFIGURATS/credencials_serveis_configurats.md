# CREDENCIALS DELS SERVEIS CONFIGURATS

## DOMINI DNS DE LA WEB
projecte-malj.servehttp.com

## FTP AMB SSL CERTIFICAT AUTOSIGNAT
**CONNEXIÓ:** projecte-malj.servehttp.com

**USUARI:** professor

**PASSWORD:** P@ssw0rd124.

## VPN (DOCUMENT PUJAT A LA ENTREGA I AL FTP)
**USUARI:** professor

**PASSWORD:** P@ssw0rd124.

## MAIL AMB SSL DE CERTIFICAT AUTOSIGNAT I SPAMASSASIN
**CORREU:** projecte-malj.servehttp.com

**USUARI 1:** pau

**PASSWORD:** P@ssw0rd124.

**USUARI 2:** roger

**PASSWORD:** P@ssw0rd124.

## FIREWALL AMB PFSENSE
**USUARI:** admin

**PASSW0RD:** pfsense

## MONITORATGE AMB NAGIOS
**USUARI:** nagiosadmin

**PASSWORD:** P@ssw0rd124.
