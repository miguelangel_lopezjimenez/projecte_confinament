# MANUAL TÈCNIC DE CONFIGURACIÓ DE CORREU (POSTFIX, DOVECOT, BUSTIES, SPAMASSASSIN I SSL)

## CONFIGURACIÓ BÀSICA DE POSTFIX

**1-** Primer començarem amb la instal·lació de Postfix, per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo apt-get install postfix
ubuntu@kyle:~$ sudo apt-get install mailutils (opcional)
~~~

**2-** Ara realitzarem la configuració inicial on tenim la possibilitat de posar el nostre domini, xarxa, mida màxima de les busties dels usuaris, etc. Per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo dpkg - reconfigure postfix
~~~

### INFORMACIÓ ADDICIONAL

El directori de configuració de **Postfix** és el següent:

~~~
/etc/postfix
~~~

I els fitxers de logs de **Postfix** es poden comprovar de la següent manera:

~~~
sudo tail -f /var/log/syslog | grep postfix
sudo tail -f /var/log/mail.err
sudo tail -f /var/log/mail.log
~~~

**3-** Una vegada instal·lat **Postfix** podem realitzar una prova en local per comprovar el seu correcte funcionament, per fer això utilitzarem la següent comanda:

~~~
telnet localhost 25
~~~

Aquestes son les comandes essencials de **Postfix** (**SMTP**):


COMANDA  |  PER A QUE SERVEIX?
--|--
EHLO  |  Serveix per obrir sessió.
MAIL FROM  |  Serveix per determinar qui envia el correu.
RCPT TO  |  Serveix per determinar a qui va dirigit el correu
DATA  |  Aquesta comanda seveix per iniciar el missatge que anem a enviar, per acabar fa falta posar un "." en una línia a part.
QUIT  |  Serveix per tancar Postfix (SMTP).
RSET  |  Serveix per avortar el missatge que anàvem a enviar.
VRFY  |  Serveix per verificar que una bustia existeix
NOOP  |  Aquesta comanda força al receptor a enviar un OK.
SEND  |  Serveix per enviar els correus en cua.
HELP  |  Aquesta comanda et mostra totes les comandes possibles.

**4-** Ara podem configurar els paràmetres desitjats de **Postfix** en _**/etc/postfix/main.cf**_

DIRECTIVA  |  DESCRICPICÓ
--|--
smtpd_banner  |  Això és el missatge de benvinguda. Si trobem $ davant una paraula vol dir que és una variable i volem el valor que contingui. Per exemple $myhostname posarà el nom del nostre equip.
myhostname  |  Estableix el nom del host del servidor
alias_maps  |  Indica on està el fitxer d'àlies de correu
alias_database  |  Indica on està el fitxer de base de dades d'àlies de correu i de quin tipus es.
myorigin  |  Indica quin es el domini d'origen. El domini a afegir per defecte al compte de correu que es fa servir.
mydestination  |  Dominis que es considerin que són el destí gestionat per aquest servidor Postfix.
relayhost  |  Adreça IP del MTA al que es reenviarà el correu d'aquest MTA.
mynetworks  |  Xarxa o xarxes i/o hosts als que està permès reenviar el seu correu per aquest MTA.
mailbox_size_limit  |  Mida màxim de bústia per usuari, el 0 = a infinit.
recipient_delimeter  |  Serveix per separar els destinataris de correu.
inet_interfaces  |  Serveix per definir les interfícies per on escolta Postfix.

**5-** Una vegada realitzat els canvis desitjats reiniciem el servei:

~~~
ubuntu@kyle:~$ sudo service postfix restart
~~~

## CONFIGURACIÓ BÀSICA DE DOVECOT

**1-** Primer començarem amb la instal·lació de **Dovecot**, per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo apt-get install dovecot-impad dovecot-pop3d
~~~

**2-** Ara iniciem el servei:

~~~
ubuntu@kyle:~$ sudo service dovecot restart
~~~

**2-** Una vegada instal·lat no fa falta res més per que funcioni amb les funcions més bàsiques, farem un test per comprovar que funciona correctament, podem fer-ho tant amb **POP3** o **IMAP**:

### COMPROVACIÓ AMB POP3

~~~
ubuntu@kyle:~$ telnet localhost pop3
~~~

### COMANDES DE POP3

COMANDA  |  DESCRIPCIÓ
--|--
USER nom_usuari  |  Serveix per indicar el nom d'usuari amb el qual volem iniciar sessió.
PASS password  |  Serveix per indicar la contrasenya del usuari amb el qual volem iniciar sessió.
STAT  |  Serveix per veure l'estat del nostre correu.
LIST  |  Serveix per veure quant correus tenim en la bústia.
RETR num_message  |  Serveix per indicar quin missatge volem llegir.
TOP num_message num_linies  |  Serveix per indicar quin missatge volem veure i quantes línies volem veure.
DELE num_message  |  Serveix per indicar quin missatge volem esborrar.
RSET  |  Serveix per recuperar els missatges esborrats.
UIDL  |  Mostra els números dels missatges.
QUIT  |  Serveix per tancar la connexió de POP3.

### COMPROVACIÓ AMB IMAP

~~~
ubuntu@kyle:~$ telnet localhost imap
~~~

COMANDA  |  DESCRIPCIÓ
--|--
LOGIN [username] [password]  |  Serveix per iniciar sessió amb l'usuari desitjat.
LIST [flags] [folder separator] [search term]  |  Serveix per llistar tots els correus.
STATUS [mailbox] [flags]  |  Serveix per veure l'estat de la bustia de correu.
SELECT [mailbox]  |  Serveix per seleccionar una bustia de correu.
FETCH [first]:[last] [flags]  | Serveix per seleccionar uns missatges de cop.
FETCH [mail number] body[header]  |  Serveix per seleccionar un missatge i el seu títol.
FETCH  [mail number]  body[text]  |  Serveix per seleccionar un missatge i el seu cos.
LOGOUT  |  Serveix per tancar la connexió de IMAP.

## CONFIGURACIÓ DE LES BÚSTIES MAILDIR

**1-** El que tenim que fer primer de tot es crear la carpeta **Maildir** en el home dels usuaris existents:

~~~
ubuntu@kyle:~$ mkdir /home/ubuntu/Maildir
~~~

**2-** Per a que els directoris es generin de manera automàtica al crear un usuari primer tenim que afegir la següent variable al arxiu **/etc/profile** o **/etc/skel/.profile**:

~~~
MAIL=/home/$USER/Maildir o MAIL=/$HOME/Maildir
~~~

**3-** Ara canviarem la bústia que utilitza **Dovecot**, per fer això tenim que anar a l'arxiu _**/etc/dovecot/conf.d/10-mail.conf**_ i afegir el següent:

### PER A MAILDIR

~~~
mail_location = maildir:~/Maildir
~~~

**NOTA:** Per defecte ve habilitat **Mailbox**, en cas de voler utilitzar **Maildir** documentar la línia de **Mailbox**.

### PER A MBOX

~~~
mail_location = mbox :~/ mail : INBOX =/ var / spool / mail /% u
~~~

**4-** Per últim reiniciem el servei per aplicar els canvis:

~~~
ubuntu@kyle:~$ sudo service dovecot restart
~~~

## CONFIGURACIÓ DE SPAMASSASSIN

**1-** Primer començarem amb la instal·lació de **Spamassassin**, per fer això utilitzarem la següent comanda:

~~~
ubuntu@kyle:~$ sudo apt-get install spamassasin spamc
~~~

**2-** Ara afegirem l'usuari **spamd** utilitzant la següent comanda:

~~~
ubuntu@kyle:~$ adduser spamd --disabled -login
~~~

**3-** Ara començarem amb la configuració de **spamassassin**, primer farem que s'actualitzi automàticament, per fer això anirem a _**/etc/default/spamassasin**_ i canviarem el següent paràmetre a 1:

~~~
CRON =1
~~~

**4-** Ara en el mateix arxiu definirem on es troba la carpeta de logs:

~~~
SAHOME ="/var/log/spamassassin/"
~~~

**5-** Ara arrancarem **spamassasin** amb la següent comanda:

~~~
ubuntu@kyle:~$ sudo service spamassassin start
~~~

**5-** Ara configurarem **Postfix** per a que utilitzi **spamassassin**, per fer això anirem al arxiu _**/etc/postfix/master.cf**_ i afegirem o modificarem aquestes línies:

### AL INICI DEL ARXIU

~~~
smtp inet n - - - - smtpd -o content_filter = spamassassin
smtps inet n - y - - smtpd -o content_filter = spamassassin
~~~

### AL FINAL DEL ARXIU

~~~
spamassassin unix - n n - - pipe user=spamd argv=/usr/bin/spamc -f -e /usr/sbin/sendmail -oi -f ${sender} ${recipient}
~~~

**6-** Si volem que **spamassasin** marqui els correus amb la puntuació d'spam anirem al arxiu _**/etc/spamassassin/local.cf**_ i deixarem el següent paràmetre així:

~~~
rewrite_header Subject ***** SPAM _SCORE_ *****
~~~

En cas de voler canviar l'score canviarem el següent paràmetre:

~~~
required_score [valor] // per exemple 3.0
~~~

**7-** Ara per aplicar tots els canvis realitzats reiniciarem el servei:

~~~
ubuntu@kyle:~$ sudo service spamassasin restart
~~~

**8-** Per últim per comprovar que **spamassassin** funciona correctament enviarem un correu amb el **gtube**, que es un test per comprovar el funcionament d'**spamassassin**:

~~~
XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X
~~~

## CONFIGURAR SSL PER A POSTFIX I DOVECOT

**1-** Primer tenim que generar el certificat i la clau que utilitzaran tant **Postfix** com **Dovecot**, per fer això executarem les següents comandes:

### GENERAR LA CLAU I LA PETICIÓ DE FIRMA DEL CERTIFICAT

~~~
ubuntu@kyle:~$ openssl req -new -newkey rsa:2048 -nodes -keyout nom_clau.key -out nom_certificat.csr
~~~

### GENERAR EL CERTIFICAT

~~~
ubuntu@kyle:~$ openssl x509 -req -days 365 -in nom_certificat.csr -signkey nom_clau.key -out nom_certificat.crt
~~~

**2-** Ara mourem tant la clau i el certificat al directori corresponent:

~~~
ubuntu@kyle:~$ cp nom_clau.key /etc/ssl/private
ubuntu@kyle:~$ cp nom_certificat.crt /etc/ssl/certs
~~~

### CONFIGURAR SSL PER A POSTFIX

**1-** Primer anirem al arxiu _**/etc/postfix/main.cf**_ i afegirem les següents línies:

~~~
# TLS parameters
smtpd_tls_cert_file=/etc/ssl/certs/nom_certificat.crt
smtpd_tls_key_file=/etc/ssl/private/nom_clau.key
smtpd_use_tls=yes
smtpd_tls_auth_only = yes
smtpd_tls_security_level = may
smtp_tls_security_level = may
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
smtpd_sasl_security_options = noanonymous, noplaintext
smtpd_sasl_tls_security_options = noanonymous
~~~

**2-** Ara anirem al arxiu _**/etc/postfix/master.cf**_ i afegirem el següent:

~~~
submission inet n       -       n       -       -       smtpd -o content_filter=spamassassin
  -o syslog_name=postfix/submission
  -o smtpd_tls_security_level=encrypt
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_sasl_type=dovecot
  -o smtpd_sasl_path=private/auth
  -o smtpd_reject_unlisted_recipient=no
  -o smtpd_client_restrictions=permit_sasl_authenticated,reject
  -o milter_macro_daemon_name=ORIGINATING

smtps     inet  n       -       n       -       -       smtpd -o content_filter=spamassassin
    -o syslog_name=postfix/smtps
    -o smtpd_tls_wrappermode=yes
    -o smtpd_sasl_auth_enable=yes
    -o smtpd_sasl_type=dovecot
    -o smtpd_sasl_path=private/auth
    -o smtpd_client_restrictions=permit_sasl_authenticated,reject
    -o milter_macro_daemon_name=ORIGINATING
~~~

### CONFIGURAR LA SSL PER A DOVECOT

**1-** Primer anirem a l'arxiu _**/etc/dovecot/conf.d/10-ssl.conf**_ i canviarem la línia **SSL** i la deixarem així:

~~~
ssl = required
~~~

**2-** En el mateix arxiu posarem les rutes dels certificats que volem utilitzar:

~~~
ssl_cert = </etc/ssl/certs/nom_certificat.crt
ssl_key = </etc/ssl/private/nom_clau.key
~~~

**3-** Ara anirem a l'arxiu _**/etc/dovecot/conf.d/10-auth.conf**_ i buscarem el següent paràmetre i l'habilitarem per deshabilitar l'autenticació de text pla:

~~~
disable_plaintext_auth = yes
~~~

**4-** Ara anirem al arxiu _**/etc/dovecot/conf.d/10-master.conf**_ i afegirem i/o modificarem els següents paràmetres:

~~~
service imap-login {
  inet_listener imap {
    port = 143
  }
  inet_listener imaps {
    port = 993
    ssl = yes
  }

  service pop3-login {
    inet_listener pop3 {
      port = 110
    }
    inet_listener pop3s {
      port = 995
      ssl = yes
    }
  }

  unix_listener auth-userdb {
      mode = 0666
      user = postfix
      group = postfix
    }

    unix_listener /var/spool/postfix/private/auth {
        mode = 0666
      }
~~~

**5-** Ara reiniciem els serveis per aplicar els canvis:

~~~
ubuntu@kyle:~$ sudo service postfix restart
ubuntu@kyle:~$ sudo service dovecot restart
~~~

**6-** Ara comprovarem a enviar un correu per el port segur:

~~~
ubuntu@kyle:~$ telnet localhost 465
~~~

**7-** Una vegada comprovat que podem enviar correus per el port segur comprovarem que podem llegir els correus per el port segur, tant en **POP3S** i **IMAPS**:

### COMPROVACIÓ SSL POP3S

~~~
ubuntu@kyle:~$ telnet locahost 995
~~~

### COMPROVACIÓ SSL IMAPS

~~~
ubuntu@kyle:~$ telnet localhost 993
~~~
